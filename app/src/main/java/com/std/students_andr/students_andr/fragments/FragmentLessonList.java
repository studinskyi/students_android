package com.std.students_andr.students_andr.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.adapters.RVAdapterLesson;
import com.std.students_andr.students_andr.models.Lesson;

import java.util.ArrayList;
import java.util.List;

public class FragmentLessonList extends Fragment {

    private List<Lesson> listLessons = new ArrayList<>();
    RecyclerView rvLessonListFragment; // вариант с использованием RecyclerView
    RVAdapterLesson adapter_RV;

    private static final String TAG = "myStudents";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lesson_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rvLessonListFragment = (RecyclerView) getActivity().findViewById(R.id.rvLessonListFragment);
        LinearLayoutManager linLM = new LinearLayoutManager(getActivity());
        rvLessonListFragment.setLayoutManager(linLM);

        adapter_RV = new RVAdapterLesson(listLessons);
        rvLessonListFragment.setAdapter(adapter_RV);
    }

    public void setListLessons(List<Lesson> listLes) {
        this.listLessons.clear();
        this.listLessons.addAll(listLes);
        if (adapter_RV != null)
            adapter_RV.notifyDataSetChanged();
        //onResume();
    }

    //    @Override
    //    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    //        super.onCreateContextMenu(menu, v, menuInfo);
    //        MenuInflater inflater = new MenuInflater(v.getContext());
    //        inflater.inflate(R.menu.menu_sms_call, menu);
    //    }
}
