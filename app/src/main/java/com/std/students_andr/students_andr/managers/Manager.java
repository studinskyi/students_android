package com.std.students_andr.students_andr.managers;

import android.content.Context;
import android.support.v4.util.Pair;

import com.std.students_andr.students_andr.models.Contact;
import com.std.students_andr.students_andr.models.ContactType;
import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Journal;
import com.std.students_andr.students_andr.models.Lesson;
import com.std.students_andr.students_andr.models.Student;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class Manager implements ManagerInterface {
    private ManagerGroups mGroups;
    private ManagerJournals mJournals;
    private ManagerLessons mLessons;
    private ManagerStudents mStudents;
    Context context;

    //    public Manager(Context context) {
    //        this.context = context;
    //        mGroups = new ManagerGroups(this);
    //        mJournals = new ManagerJournals(this);
    //        mLessons = new ManagerLessons(this);
    //        mStudents = new ManagerStudents(this, context);
    //
    //        students = new ArrayList<>();
    //    }

    // реализация синглтона менеджера через класс-холдер
    public Manager() {
        //students = new ArrayList<>();
    }

    private static class ManagerHolder {
        private final static Manager instance = new Manager();
    }

    public static Manager getInstance() {
        return ManagerHolder.instance;
    }


    public Group addGroup(String name, int number) {
        return mGroups.addGroup(name, number);
    }

    @Override
    public Object addGroup(Object obj, int numberGroup) {
        // переопределённый для использования в Proxy тестах
        return mGroups.addGroup((String) obj, numberGroup);
    }

    public Group findGroup(int numberGroup) {
        return mGroups.findGroup(numberGroup); // возвращает null если группа по искомому полю не была найдена
    }

    public Group findGroup(int numberGroup, String name) {
        return mGroups.findGroup(numberGroup, name); // возвращает null если группа по искомому полю не была найдена
    }

    public Group findGroup(String name) {
        return mGroups.findGroup(name); // возвращает null если группа по искомому полю не была найдена
    }

    public Long getGroupIdByNumberGroup(int numberGroup) {
        return mGroups.getGroupIdByNumberGroup(numberGroup); // возвращает (long) 0 если группа по искомому полю не была найдена
    }

    public Group getGroupByNameGroup(String name) {
        return mGroups.getGroupByNameGroup(name); // возвращает null если группа по искомому полю не была найдена
    }

    public Group getGroupByStudent(Student student) {
        return mGroups.getGroupByStudent(student);
    }

    public Group getGroupById(Long groupId) {
        return mGroups.getGroupById(groupId);
    }

    public List<Group> getGroupsListFilteredByName(String name) {
        return mGroups.getGroupsListFilteredByName(name);
    }

    public HashMap<Long, Group> getGroups() {
        return mGroups.getGroups();
    }

    public List<Group> getGroupsList() {
        return mGroups.getGroupsList();
    }

    public HashMap<Long,Student> getStudentsInGroup(Group group) {
        //        // возвращение динамического списка студентов из HashMap в группе
        //        HashMap<Long, Student> students = group.getStudents();
        //        List<Student> list = new ArrayList<Student>(students.values());
        return mGroups.getStudentsInGroup(group);
    }

    public List<Student> getStudentsListInGroup(Group group) {
        // для промежуточного хранения списка студентов группы (используется при отображении адаптером списков)
        return mGroups.getStudentsListInGroup(group);
    }

    public List<Student> getStudentsListAll() {
        return mStudents.getStudentsListAll();
    }

    public List<Student> getStudentsListAllFromDB() {
        return mStudents.getStudentsListAllFromDB();
    }

    public List<Student> getStudentListFilteredByFirstName(String firstName) {
        return mStudents.getStudentListFilteredByFirstName(firstName);
    }

    public List<Student> getStudentListFilteredBySurname(String surname) {
        return mStudents.getStudentListFilteredBySurname(surname);
    }

    public List<Lesson> getLessonListFilteredByName(String nameLes) {
        return mLessons.getLessonListFilteredByName(nameLes);
    }

    public int getAmountOfStudnts() {
        return mStudents.getAmountOfStudnts();
    }

    //    public Student addStudent(Calendar dateOfBirth, Long groupID, String firstName,
    //                              String surname, String secondName) {
    //        return mStudents.addStudent(dateOfBirth, groupID, firstName, surname, secondName);
    //    }

    public Student addNewStudentToGroup(Calendar dateOfBirth, String firstName,
                                        String surname, String secondName, Group group) {
        return mGroups.addNewStudentToGroup(dateOfBirth, firstName, surname, secondName, group);
    }

    public Student getStudent(String firstName, String surname, String secondName) {
        return mStudents.getStudent(firstName, surname, secondName);
    }

    public void addStudentToGroup(Student student, Group group) {
        mGroups.addStudentToGroup(student, group);
    }

    // добавление списка студентов в гуппу
    public HashMap<Long, Student> addStudentsToGroup(Group group, List<Student> listStudents) {
        return mStudents.addStudentsToGroup(group, listStudents); // возврат результирующего HashMap студентов группы
    }

    public List<Contact> addContactStudent(ContactType contactType, String contactValue, Student student) {
        return mStudents.addContactStudent(contactType, contactValue, student);
    }

    public List<Contact> getContacts(Student student) {
        return mStudents.getContacts(student);
    }


    // функция проверки наличия студента в группе
    public boolean isStudentInGroup(Group group, Student student) {
        return mStudents.isStudentInGroup(group, student);
    }

    public void removeStudent(Student student) {
        mStudents.removeStudent(student);
    }

    public Student getStudentById(Long studentId) {
        return mStudents.getStudentById(studentId);
    }

    public Lesson addLesson(String name, Calendar startTime, Integer duration,
                            String room, String description, String subject, String lector, Group group) {
        return mLessons.addLesson(name, startTime, duration, room, description, subject, lector, group);
    }

    public HashMap<Long, Lesson> addLesson(Lesson lesson) {
        return mLessons.addLesson(lesson);
    }

    public List<Lesson> getLessonsListAll() {
        return mLessons.getLessonsListAll();
    }

    public List<Lesson> getLessonListForGroup(Group group) {
        return mLessons.getLessonListForGroup(group);
    }

    public HashMap<Long, Lesson> getLessons() {
        return mLessons.getLessons();
    }

    public Lesson getLessonById(Long lessonId) {
        return mLessons.getLessonById(lessonId);
    }

    public Journal addJournal(String name) {
        return mJournals.addJournal(name);
    }

    public Journal findJournal(String name) {
        return mJournals.findJournal(name);
    }

    // добавление одного конкретно взятого студента в журнал
    public boolean addStudentToJournal(Journal journal, Student student) {
        return mJournals.addStudentToJournal(journal, student);
    }

    // добавление списка студентов в журнал (сюда же входит вариант добавления списка студентов группы)
    public HashMap<Student, Boolean> addStudentsToJournal(Journal journal, HashMap<Long, Student> students) {
        return mJournals.addStudentsToJournal(journal, students); // возврат результирующего HashMap присутствия студентов
    }

    public Journal getJournalById(Long journalId) {
        return mJournals.getJournalById(journalId);
    }

    public List<Journal> getJournalsListAll() {
        return mJournals.getJournalsListAll();
    }

    public List<Pair<Pair<Student, Lesson>, Boolean>> getJournalsLessonsPresentsAll() {
        return mJournals.getJournalsLessonsPresentsAll();
    }

    public HashMap<Long, Journal> getJournals() {
        return mJournals.getJournals();
    }

    public ManagerGroups get_mGroups() {
        return mGroups;
    }

    public ManagerJournals get_mJournals() {
        return mJournals;
    }

    public ManagerLessons get_mLessons() {
        return mLessons;
    }

    public ManagerStudents get_mStudents() {
        return mStudents;
    }

    public void setmGroups(ManagerGroups mGroups) {
        this.mGroups = mGroups;
    }

    public void setmJournals(ManagerJournals mJournals) {
        this.mJournals = mJournals;
    }

    public void setmLessons(ManagerLessons mLessons) {
        this.mLessons = mLessons;
    }

    public void setmStudents(ManagerStudents mStudents) {
        this.mStudents = mStudents;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
