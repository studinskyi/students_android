package com.std.students_andr.students_andr.activities.add;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.std.students_andr.students_andr.R;

public class AddLessonActivity extends AppCompatActivity  implements View.OnClickListener {

    private AlertDialog dialog;
    Button btnSetDateLesson;
    EditText etDateLesson;

    int DIALOG_DATE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lesson);

        etDateLesson = (EditText) findViewById(R.id.etDateLesson);

        btnSetDateLesson = (Button) findViewById(R.id.btnSetDateLesson);
        btnSetDateLesson.setOnClickListener(this);



    }

//    // отображаем диалоговое окно для выбора даты
//    public void setDate(View v) {
//        new DatePickerDialog(MainActivity.this, d,
//                dateAndTime.get(Calendar.YEAR),
//                dateAndTime.get(Calendar.MONTH),
//                dateAndTime.get(Calendar.DAY_OF_MONTH))
//                .show();
//    }

//    // установка обработчика выбора времени
//    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
//        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            dateAndTime.set(Calendar.MINUTE, minute);
//            setInitialDateTime();
//        }
//    };


//    @Override
//    protected Dialog onCreateDialog(int id) {
//        return super.onCreateDialog(id);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSetDateLesson:
                DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        etDateLesson.setText(i + " " + i1 + " " + i2);
                        //Toast.makeText(AddLessonActivity.this, i + " " + i1 + " " + i2, Toast.LENGTH_LONG).show();
                    }
                };
                DatePickerDialog datePeacker = new DatePickerDialog(this, myDateListener, 2017, 6, 30);
                datePeacker.show();
                Toast.makeText(AddLessonActivity.this, "show datePeacker", Toast.LENGTH_LONG).show();
                break;
            case R.id.btnSetTimeLesson:
                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        Toast.makeText(AddLessonActivity.this, i + " " + i1, Toast.LENGTH_LONG).show();
                    }
                };
                TimePickerDialog timePicker = new TimePickerDialog(this, myTimeListener, 10, 13, false);
                timePicker.show();
                break;
        }
    }
}
