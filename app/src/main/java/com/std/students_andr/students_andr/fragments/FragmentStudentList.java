package com.std.students_andr.students_andr.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.adapters.RVAdapterStudent;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.List;

public class FragmentStudentList extends Fragment {

    private List<Student> listStudents = new ArrayList<>();
    RecyclerView rvStudentListFragment; // вариант с использованием RecyclerView
    RVAdapterStudent adapter_RV;

    private static final String TAG = "myStudents";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //        return super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_student_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rvStudentListFragment = (RecyclerView) getActivity().findViewById(R.id.rvStudentListFragment);
        LinearLayoutManager linLM = new LinearLayoutManager(getActivity());
        rvStudentListFragment.setLayoutManager(linLM);

        adapter_RV = new RVAdapterStudent(listStudents);
        rvStudentListFragment.setAdapter(adapter_RV);
    }

    public void setListStudents(List<Student> listS) {
        this.listStudents.clear();
        this.listStudents.addAll(listS);
        if (adapter_RV != null)
            adapter_RV.notifyDataSetChanged();
        //onResume();
    }

    //    @Override
    //    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    //        super.onCreateContextMenu(menu, v, menuInfo);
    //        MenuInflater inflater = new MenuInflater(v.getContext());
    //        inflater.inflate(R.menu.menu_sms_call, menu);
    //    }
}
