package com.std.students_andr.students_andr.activities;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.element.ProfileActivity;
import com.std.students_andr.students_andr.activities.list.GroupsActivity;
import com.std.students_andr.students_andr.activities.list.JournalsActivity;
import com.std.students_andr.students_andr.activities.list.LessonsActivity;
import com.std.students_andr.students_andr.activities.list.StudentListActivity;
import com.std.students_andr.students_andr.activities.list.StudentsActivity;
import com.std.students_andr.students_andr.managers.Manager;
import com.std.students_andr.students_andr.managers.ManagerGroups;
import com.std.students_andr.students_andr.managers.ManagerJournals;
import com.std.students_andr.students_andr.managers.ManagerLessons;
import com.std.students_andr.students_andr.managers.ManagerStudents;
import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Journal;
import com.std.students_andr.students_andr.models.Lesson;
import com.std.students_andr.students_andr.models.Student;
import com.std.students_andr.students_andr.models.User;
import com.std.students_andr.students_andr.models.sqlite.DBHelper;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private Button btnLogout;
    private Button btnProfileUser;
    private Button btnOpenGroupListAct;
    private Button btnOpenStudentListAct;
    private Button btnOpenStudentsFromDB;
    private Button btnOpenJournalsListAct;
    private Button btnOpenLessonsListAct;

    private TextView tvCaption;
    //private Context context;
    private static User userCurrent = null; // текущий активный пользователь
    //private static boolean isUserAdministrator;
    private static HashMap<String, User> users = new HashMap<>(); // список юзеров
    private static String loginCurrent;
    private static String passwordCurrent;
    private static final String logTAG = "myStudents";

    static Context context = null;
    public static Manager manager;
    //private DBHelper dbHelper;

    //    {
    //        initializeTestData(getBaseContext());
    //    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        //manager = new Manager(context);
        manager = Manager.getInstance(); // инициализация глобального менеджера уровня Presenter через паттерн синглтон
        manager.setContext(context);
        manager.setmGroups(new ManagerGroups(manager));
        manager.setmStudents(new ManagerStudents(manager, context));
        manager.setmLessons(new ManagerLessons(manager));
        manager.setmJournals(new ManagerJournals(manager));

        // инициализация тестовых таблиц (ДЛЯ ОТЛАДКИ)
        initializeTestData(getBaseContext());

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnProfileUser = (Button) findViewById(R.id.btnProfileUser);
        btnOpenGroupListAct = (Button) findViewById(R.id.btnOpenGroupListAct);
        btnOpenStudentListAct = (Button) findViewById(R.id.btnOpenStudentListAct);
        btnOpenStudentsFromDB = (Button) findViewById(R.id.btnOpenStudentsFromDB);
        btnOpenJournalsListAct = (Button) findViewById(R.id.btnOpenJournalsListAct);
        btnOpenLessonsListAct = (Button) findViewById(R.id.btnOpenLessonsListAct);

        tvCaption = (TextView) findViewById(R.id.tvCaption);

        btnLogin.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnProfileUser.setOnClickListener(this);
        btnOpenGroupListAct.setOnClickListener(this);
        btnOpenStudentListAct.setOnClickListener(this);
        btnOpenStudentsFromDB.setOnClickListener(this);
        btnOpenJournalsListAct.setOnClickListener(this);
        btnOpenLessonsListAct.setOnClickListener(this);

        // регистрация тестового пользователя администратора (ДЛЯ ОТЛАДКИ)
        String loginAdminTest = "login_admin";
        String passwordAdminTest = "password_admin";
        MainActivity.registrationNewUser(loginAdminTest, passwordAdminTest, true);
        LoginActivity.login(loginAdminTest, passwordAdminTest, this);
//        // регистрация тестового пользователя администратора (ДЛЯ ОТЛАДКИ)
//        String loginStudentTest = "login_student";
//        String passwordStudentTest = "password_admin";
//        MainActivity.registrationNewUser(loginStudentTest, passwordStudentTest, false);
//        LoginActivity.login(loginStudentTest, passwordStudentTest, this);

        // стартовое заполнение данными
        if (manager.getStudentsListAll().size() == 0) {
            DBHelper dbHelper = new DBHelper(context, "studentdb", null, 1);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            database.execSQL("INSERT INTO students (firstname,surname,secondname) VALUES('M11','ich11','lo11');");
            database.execSQL("INSERT INTO students (firstname,surname,secondname) VALUES('M22','ich22','lo22');");
            database.execSQL("INSERT INTO students (firstname,surname,secondname) VALUES('M33','ich33','lo33');");
            dbHelper.close();
        }

        setVisibleElementsView();
    }

    @Override
    public void onClick(View v) {
        // по id определяем кнопку, вызвавшую этот обработчик
        switch (v.getId()) {
            case R.id.btnLogin:
                Log.d(logTAG, "Нажата кнопка Login в activity_main");
                //tvOut.setText("Нажата кнопка ОК");
                //Toast.makeText(this, "Нажата кнопка ОК", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, LoginActivity.class));
                break;
            case R.id.btnLogout:
                Log.d(logTAG, "Нажата кнопка Logout в activity_main");
                // сброс всей информаци об активном пользователе (по причине неверного ввода пароля и логина)
                MainActivity.setUserCurrent(null);
                MainActivity.setLoginCurrent("");
                MainActivity.setPasswordCurrent("");
                //Toast.makeText(this, "Нажата кнопка ОК", Toast.LENGTH_SHORT).show();
                onResume();
                //startActivity(new Intent(context, MainActivity.class));
                break;
            case R.id.btnProfileUser:
                Log.d(logTAG, "Нажата кнопка Profile в activity_main");
                //tvOut.setText("Нажата кнопка ОК");
                //Toast.makeText(this, "Нажата кнопка Profile в activity_main", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, ProfileActivity.class));
                break;
            case R.id.btnOpenGroupListAct:
                Log.d(logTAG, "Нажата кнопка OpenGroupListAct в activity_main");
                // запускает Activity в новом таске. Если уже существует таск с экземпляром данной Activity, то этот таск становится активным, и срабатываем метод onNewIntent().
                Intent intentGroupList = new Intent(context, GroupsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentGroupList.putExtra("substrNameGroup", ""); // можно передавать фильтр по подстроке имени группы
                //intentGroupList.putExtra("isUserAdministrator", isUserAdministrator);
                startActivity(intentGroupList);

                //startActivity(new Intent(context, GroupsActivity.class));
                break;
            case R.id.btnOpenStudentListAct:
                Log.d(logTAG, "Нажата кнопка OpenStudentListAct в activity_main");
                Intent intentStudentList = new Intent(context, StudentsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentStudentList.putExtra("substrSurnameStudent", ""); // по умолчанию подстрока поиска равна "rov" (Petrov, Sidorov)
                intentStudentList.putExtra("nameChoiseGroup", ""); // для пользователя с полными правами отбор по группе не нужен
                //intentStudentList.putExtra("isUserAdministrator", isUserAdministrator);
                startActivity(intentStudentList);
                //startActivity(new Intent(context, StudentsActivity.class));
                break;
            case R.id.btnOpenStudentsFromDB:
                Log.d(logTAG, "Нажата кнопка btnOpenStudentsFromDB в activity_main");
                Intent intentStudentsFromDB = new Intent(context, StudentListActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentStudentsFromDB.putExtra("substrSurnameStudent", ""); // по умолчанию подстрока поиска равна "rov" (Petrov, Sidorov)
                intentStudentsFromDB.putExtra("nameChoiseGroup", ""); // для пользователя с полными правами отбор по группе не нужен
                startActivity(intentStudentsFromDB);
                break;
            case R.id.btnOpenJournalsListAct:
                Log.d(logTAG, "Нажата кнопка OpenJournalsListAct в activity_main");
                //                Intent intentGournalList = new Intent(context, JournalListActivity.class);
                //                intentGournalList.putExtra("substrNameJournal", "фил");
                //                intentGournalList.putExtra("isUserAdministrator", isUserAdministrator);
                //                startActivity(intentGournalList);
                //startActivity(new Intent(context, GroupsActivity.class));

                Intent intentJournalList = new Intent(context, JournalsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //                intentJournalList.putExtra("substrJournalFilter", ""); // можно передавать фильтр по подстроке названия предмета или фамилии студента
                //                intentJournalList.putExtra("studentId", ""); // возможна передача id студента (для фильтрации журналов по студенту)
                startActivity(intentJournalList);

                //startActivity(new Intent(context, JournalListActivity.class));
                break;
            case R.id.btnOpenLessonsListAct:
                Log.d(logTAG, "Нажата кнопка OpenLessonsListAct в activity_main");
                Intent intentLessonList = new Intent(context, LessonsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentLessonList.putExtra("substrNameLesson", ""); // по умолчанию подстрока поиска в наименовании занятий
                intentLessonList.putExtra("nameChoiseGroup", ""); // для пользователя с полными правами отбор по группе не нужен
                startActivity(intentLessonList);
                //startActivity(new Intent(context, LessonsActivity.class));
                break;
        }

    }

    public static User registrationNewUser(String login, String password, boolean isAdministrator) {
        User userNew = new User(login, password, isAdministrator);
        users.put(login, userNew);
        Log.d(logTAG, "зарегистрирован новый пользователь login: " + login + " password: " + password);
        return userNew;
    }

    public static User getUserByLogin(String login) {
        //        for (HashMap.Entry<String, User> entry : users.entrySet())
        //            if (entry.getValue().getName().equals(name))
        //                return entry.getValue();
        return users.get(login); // возвращает null если пользователь по логину не найден
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "onResume() в активити activity_main", Toast.LENGTH_SHORT).show();
        //isUserAdministrator = false;

        setVisibleElementsView();
    }

    private void setVisibleElementsView() {
        if (userCurrent != null) {
            //isUserAdministrator = userCurrent.isAdministrator();

            tvCaption.setText("Hello " + userCurrent.getLogin() + "!");
            btnLogin.setVisibility(View.INVISIBLE);
            btnLogout.setVisibility(View.VISIBLE);
            //            btnLogin.setEnabled(false);
            //            btnLogout.setEnabled(true);
        } else {
            // пользователь не авторизован
            tvCaption.setText("Hello students!");
            btnLogin.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.INVISIBLE);
            //            btnLogin.setEnabled(true);
            //            btnLogout.setEnabled(false);
        }

        if (isUserAdministrator()) {
            btnOpenGroupListAct.setEnabled(true);
            btnOpenStudentListAct.setEnabled(true);
            btnOpenJournalsListAct.setEnabled(true);
            btnOpenLessonsListAct.setEnabled(true);
            //            btnOpenGroupListAct.setVisibility(View.VISIBLE);
        } else {
            btnOpenGroupListAct.setEnabled(false);
            btnOpenStudentListAct.setEnabled(false);
            btnOpenJournalsListAct.setEnabled(false);
            btnOpenLessonsListAct.setEnabled(false);
            //            btnOpenGroupListAct.setVisibility(View.INVISIBLE);
        }
        // перевод в недоступны режим ряда кнопок
        btnProfileUser.setEnabled(false); // делаем кнопку открытия профиля недоступной, она будет открываться для каждого студента
        //btnOpenJournalsListAct.setEnabled(false);
    }


    public static User getUserCurrent() {
        return userCurrent;
    }

    public static void setUserCurrent(User userCurrent) {
        MainActivity.userCurrent = userCurrent;
    }

    public static String getLoginCurrent() {
        return loginCurrent;
    }

    public static void setLoginCurrent(String loginCurrent) {
        MainActivity.loginCurrent = loginCurrent;
    }

    public static String getPasswordCurrent() {
        return passwordCurrent;
    }

    public static void setPasswordCurrent(String passwordCurrent) {
        MainActivity.passwordCurrent = passwordCurrent;
    }

    public static boolean isUserAdministrator() {
        if (userCurrent != null)
            return userCurrent.isAdministrator();

        return false;
    }

    public static HashMap<String, User> getUsers() {
        return users;
    }

    private static void initializeTestData(Context context) {
        // создание тестового списка групп из текстового массива ресурсов testData_list_groups
        String[] resGroupsTest = context.getResources().getStringArray(R.array.testData_list_groups);
        for (String s : resGroupsTest) {
            //String artistpicked = extras.getString("artist");
            String[] arrItemGroup = s.split(">>");
            String strNameItem = arrItemGroup[0].trim();
            String strNumberGroupItem = arrItemGroup[1].trim();
            //String setRes = String.valueOf(getRes);
            Integer numberGroup = 0;
            try {
                numberGroup = Integer.parseInt(strNumberGroupItem);
            } catch (NumberFormatException e) {
                //numberGroup = 0;
            }
            MainActivity.manager.addGroup(strNameItem, numberGroup);

            //            firstW[0] = firstW[0].trim();
            //            String albumSearch = firstW[0] + "_code";
            //            int getRes = getResources().getIdentifier(albumSearch, "string", getPackageName());
            //            String setRes = String.valueOf(getRes);
            //            int getRes2 = getResources().getIdentifier(setRes, "array", getPackageName());
            //            String[] albums = getResources().getStringArray(getRes2);
            //            setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, albums));
            //            TextView t = (TextView) findViewById(R.id.albumName);
            //            t.setText(setRes);
        }

        // создание тестового списка студентов из текстового массива ресурсов testData_list_groups
        String[] resStudentTest = context.getResources().getStringArray(R.array.testData_list_students);
        for (String s : resStudentTest) {
            String[] arrItemStudent = s.split(">>");
            String strDateOfBirth = arrItemStudent[0].trim();
            String[] arrDate = strDateOfBirth.split(".");
            Calendar dateOfBirth = null;
            try {
                dateOfBirth = new GregorianCalendar(Integer.parseInt(arrDate[0]), Integer.parseInt(arrDate[1]) - 1, Integer.parseInt(arrDate[2]));
            } catch (Exception e) {
                dateOfBirth = new GregorianCalendar(1990, Calendar.JANUARY, 1);
            }
            String strFirstName = arrItemStudent[1].trim();
            String strSurname = arrItemStudent[2].trim();
            String strSecondName = arrItemStudent[3].trim();
            String strNameGroup = arrItemStudent[4].trim();

            Group gr = MainActivity.manager.getGroupByNameGroup(strNameGroup);
            MainActivity.manager.addNewStudentToGroup(dateOfBirth, strFirstName, strSurname, strSecondName, gr);
            //            <string-array name="testData_list_students">
            //        <item>"1994.02.08>>Petr>>Petrov>>Petrovich>>stc01"</item>
        }

        // создание объектов групп
        Group gr1 = MainActivity.manager.addGroup("группа 1", 1);
        Group gr2 = MainActivity.manager.addGroup("группа 2", 2);
        Group gr3 = MainActivity.manager.addGroup("группа 3", 3);

        Lesson les1 = MainActivity.manager.addLesson("History", new GregorianCalendar(2017, Calendar.JUNE, 10, 9, 30), 60, "кабинет 101", "История древних времен", "Древняя Русь", "Сидоров И.П.", gr1);
        Lesson les2 = MainActivity.manager.addLesson("Philosophy", new GregorianCalendar(2017, Calendar.AUGUST, 16, 10, 30), 60, "кабинет 202", "Труды немецких философов", "Философия капитала", "Карл Маркс", gr2);
        Lesson les3 = MainActivity.manager.addLesson("Philology", new GregorianCalendar(2017, Calendar.SEPTEMBER, 7, 11, 30), 60, "кабинет 303", "Романо-германская филология", "Зарубежная филология", "Газизов Рафаэль", gr3);
        Lesson les4 = MainActivity.manager.addLesson("Android", new GregorianCalendar(2017, Calendar.JUNE, 6, 12, 30), 60, "кабинет 501", "Android Activity", "Android Views", "Первушов А.В.", gr3);

        Student st1 = MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1994, Calendar.FEBRUARY, 8), "Petr2", "Petrov2", "Petrovich2", gr1);
        Student st2 = MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1980, Calendar.NOVEMBER, 25), "Ivan2", "Ivanov2", "Ivanovich2", gr2);
        Student st3 = MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1997, Calendar.MAY, 16), "Alexandr2", "Alexandrov2", "Alexandrovich2", gr3);
        Student st4 = MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1975, Calendar.APRIL, 14), "Viktor2", "Viktorov2", "Viktorovich2", gr3);

        Journal jrn1 = MainActivity.manager.addJournal("журнал 2017");
        HashMap<Student, HashMap<Lesson, Boolean>> presentHashAll = jrn1.getPresentStudents();
        HashMap<Lesson, Boolean> presentStudent = new HashMap<>();
        presentStudent = new HashMap<>();
        presentStudent.put(les1, true);
        presentStudent.put(les2, false);
        presentStudent.put(les3, true);
        presentStudent.put(les4, true);
        presentHashAll.put(st1, presentStudent);
        presentStudent = new HashMap<>();
        presentStudent.put(les1, true);
        presentStudent.put(les2, false);
        presentStudent.put(les3, true);
        presentStudent.put(les4, true);
        presentHashAll.put(st2, presentStudent);
        presentStudent = new HashMap<>();
        presentStudent.put(les1, true);
        presentStudent.put(les2, false);
        presentStudent.put(les3, true);
        presentStudent.put(les4, true);
        presentHashAll.put(st3, presentStudent);


        //        // добавление студентов
        //        MainActivity.manager.addNewStudentToGroup(pUtil.calend(1994, Calendar.FEBRUARY, 8), "Petr", "Petrov", "Petrovich", choiceGroup);
        //        MainActivity.manager.addNewStudentToGroup(pUtil.calend(1980, Calendar.NOVEMBER, 25), "Ivan", "Ivanov", "Ivanovich", choiceGroup);
        //        MainActivity.manager.addNewStudentToGroup(pUtil.calend(1997, Calendar.MAY, 16), "Alexandr", "Alexandrov", "Alexandrovich", choiceGroup);
        //        MainActivity.manager.addNewStudentToGroup(pUtil.calend(1975, Calendar.APRIL, 14), "Viktor", "Viktorov", "Viktorovich", choiceGroup);

        //        Lesson les1 = MainActivity.manager.addLesson("История - 10 июня 2017", new GregorianCalendar(2017, Calendar.JUNE, 10), 60, "кабинет 101", "История древних времен", "Древняя Русь", "Сидоров И.П.", gr1);
        //        Lesson les2 = MainActivity.manager.addLesson("Философия - 16 августа 2017", new GregorianCalendar(2017, Calendar.AUGUST, 16), 60, "кабинет 202", "Труды немецких философов", "Философия капитала", "Карл Маркс", gr2);
        //        Lesson les3 = MainActivity.manager.addLesson("Филология - 7 сентября 2017", new GregorianCalendar(2017, Calendar.SEPTEMBER, 7), 60, "кабинет 303", "Романо-германская филология", "Зарубежная филология", "Газизов Рафаэль", gr3);
        //        Lesson les4 = MainActivity.manager.addLesson("Android - 6 июня 2017", new GregorianCalendar(2017, Calendar.JUNE, 6), 60, "кабинет 501", "Android Activity", "Android Views", "Первушов А.В.", gr3);

        //        Java Core, Инструменты промышленной разработки ПО, Паттерны GoF,
        //                Android Application Core, Android Context , Android Activity, Android Views,
        //                Adapters,  Android Fragments,  Android Intent, Filters, Android Service Model,
        //        Передача данных между приложениями Android,  Работа с данными в Android (БД, файлы),
        //        Отладка и профилирование android-приложений, Написание автоматизированных тестов для android-приложений,
        //                Android Material Design.

    }

    public static String getLogTAG() {
        return logTAG;
    }
}
