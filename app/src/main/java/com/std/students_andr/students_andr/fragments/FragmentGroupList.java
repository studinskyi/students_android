package com.std.students_andr.students_andr.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.adapters.RVAdapterGroup;
import com.std.students_andr.students_andr.models.Group;

import java.util.ArrayList;
import java.util.List;

public class FragmentGroupList extends Fragment {

    private List<Group> listGroups = new ArrayList<>();
    RecyclerView rvGroupListFragment;
    RVAdapterGroup adapter_RV;

    private static final String TAG = "myStudents";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_group_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rvGroupListFragment = (RecyclerView) getActivity().findViewById(R.id.rvGroupListFragment);
        LinearLayoutManager linLM = new LinearLayoutManager(getActivity());
        rvGroupListFragment.setLayoutManager(linLM);

        adapter_RV = new RVAdapterGroup(listGroups);
        rvGroupListFragment.setAdapter(adapter_RV);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        Log.d(TAG, "onResume - FragmentGroupList");
//        //        // учет строки фильтра при выводе списка групп
//        //        nameGroupFilter = etNameGroupFilter.getText().toString();
//        //        filterListGroupByName(nameGroupFilter);
//        //filterListGroupByName("");
//        adapter_RV.notifyDataSetChanged();
//    }

    //    @Override
    //    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    //        super.onCreateContextMenu(menu, v, menuInfo);
    //    }

    //    private void filterListGroupByName(String name) {
    //        listGroups.clear();
    //        if (name.equals("") || name == null) {
    //            // подбор всего списка групп (без фильтрации)
    //            for (Group group : MainActivity.manager.getGroupsList())
    //                listGroups.add(group);
    //        } else {
    //            // получение группы по наименованию
    //            //Group findGroup = MainActivity.manager.getGroupByNameGroup(name);
    //            for (Group group : MainActivity.manager.getGroupsListFilteredByName(name))
    //                listGroups.add(group);
    //            //            if (findGroup != null)
    //            //                listGroups.add(findGroup);
    //        }
    //    }

    public void setListGroups(List<Group> listG) {
        this.listGroups.clear();
        this.listGroups.addAll(listG);
        if (adapter_RV != null)
            adapter_RV.notifyDataSetChanged();
        //onResume();
    }

    //    @Override
    //    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    //        super.onCreateContextMenu(menu, v, menuInfo);
    //        MenuInflater inflater = new MenuInflater(v.getContext());
    //        inflater.inflate(R.menu.menu_sms_call, menu);
    //    }


}
