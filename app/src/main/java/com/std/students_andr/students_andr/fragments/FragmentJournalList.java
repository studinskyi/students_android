package com.std.students_andr.students_andr.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.adapters.RVAdapterJournal;
import com.std.students_andr.students_andr.models.Lesson;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.List;

public class FragmentJournalList extends Fragment {
    //    private List<Journal> listJournals = new ArrayList<>();
    //    private List<Pair<Lesson, Boolean>> lessonsPresents = new ArrayList<>();
    private List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents = new ArrayList<>();
    RecyclerView rvJournalListFragment; // вариант с использованием RecyclerView
    RVAdapterJournal adapter_RV;

    private static final String TAG = "myStudents";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //        return super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_journal_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rvJournalListFragment = (RecyclerView) getActivity().findViewById(R.id.rvJournalListFragment);
        LinearLayoutManager linLM = new LinearLayoutManager(getActivity());
        rvJournalListFragment.setLayoutManager(linLM);

        adapter_RV = new RVAdapterJournal(lessonsPresents);
        rvJournalListFragment.setAdapter(adapter_RV);
    }

    public void setLessonsPresentsList(List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPres) {
        this.lessonsPresents.clear();
        this.lessonsPresents.addAll(lessonsPres);
        if (adapter_RV != null)
            adapter_RV.notifyDataSetChanged();
        //onResume();
    }

    //    @Override
    //    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    //        super.onCreateContextMenu(menu, v, menuInfo);
    //        MenuInflater inflater = new MenuInflater(v.getContext());
    //        inflater.inflate(R.menu.menu_sms_call, menu);
    //    }
}
