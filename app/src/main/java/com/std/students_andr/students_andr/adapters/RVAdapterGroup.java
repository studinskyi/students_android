package com.std.students_andr.students_andr.adapters;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.list.StudentsActivity;
import com.std.students_andr.students_andr.models.Group;

import java.util.List;

public class RVAdapterGroup extends RecyclerView.Adapter<RVAdapterGroup.GroupViewHolder> {
    private List<Group> listGroups;
    private static Group choiseGroup;

    public RVAdapterGroup(List<Group> listGroups) {
        this.listGroups = listGroups;
    }

    public class GroupViewHolder extends RecyclerView.ViewHolder {
        TextView nameGroup;
        TextView numberGroup;
        private ActionMode mActionMode;

        public GroupViewHolder(final View itemView) {
            super(itemView);

            nameGroup = (TextView) itemView.findViewById(R.id.tvGroupItemName);
            numberGroup = (TextView) itemView.findViewById(R.id.tvGroupItemNumber);

            // 1-й вариант вызова контекстного меню через "Использование режима контекстных действий"
            final ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.menu_sms_call, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                // Called when the user selects a contextual menu item
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    Toast.makeText(itemView.getContext(), "выбран пункт меню " + listGroups.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
                    switch (item.getItemId()) {
                        case R.id.miSendSMS:
                            Intent intentSendSMS = new Intent(Intent.ACTION_SENDTO);
                            intentSendSMS.setData(Uri.parse("smsto:+79124277403"));
                            intentSendSMS.putExtra("sms_body", "text sms");
                            intentSendSMS.resolveActivity(itemView.getContext().getPackageManager());
                            itemView.getContext().startActivity(intentSendSMS);
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        case R.id.miCall:
                            Intent intentCall = new Intent(Intent.ACTION_DIAL);
                            intentCall.setData(Uri.parse("tel:+79102366669"));
                            intentCall.resolveActivity(itemView.getContext().getPackageManager());
                            itemView.getContext().startActivity(intentCall);
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    mActionMode = null;
                }
            };

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                // Called when the user long-clicks on someView
                public boolean onLongClick(View view) {
                    if (mActionMode != null) {
                        return false;
                    }

                    // Start the CAB using the ActionMode.Callback defined above
                    mActionMode = ((Activity) itemView.getContext()).startActionMode(mActionModeCallback);
                    view.setSelected(true);
                    return true;
                }
            });

            // 2-й вариант вызова контекстного меню через переопределение  itemView.setOnCreateContextMenuListener
            //            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            //                @Override
            //                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            //                    MenuItem itemCall = menu.add("call " + listGroups.get(getAdapterPosition()).getName());
            //                    itemCall.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            //                        @Override
            //                        public boolean onMenuItemClick(MenuItem item) {
            //                            Intent myIntent = new Intent(Intent.ACTION_DIAL);
            //                            myIntent.setData(Uri.parse("tel:+79102366669"));
            //                            myIntent.resolveActivity(itemView.getContext().getPackageManager());
            //                            itemView.getContext().startActivity(myIntent);
            //                            return true;
            //                        }
            //                    });
            //                    //Toast.makeText(itemView.getContext(), "выбран пункт меню " + listGroups.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
            //                }
            //            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    //Toast.makeText(itemView.getContext(), "выбрана группа " + listGroups.get(pos), Toast.LENGTH_SHORT).show();
                    choiseGroup = listGroups.get(pos);
                    //StudentsActivity.setChoiceGroup(choiseGroup);
                    Intent intentStudentList = new Intent(itemView.getContext(), StudentsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentStudentList.putExtra("substrFirstNameStudent", ""); // подстрока фильтра студентов
                    intentStudentList.putExtra("nameChoiseGroup", choiseGroup.getName()); // для пользователя с полными правами отбор по группе не нужен
                    itemView.getContext().startActivity(intentStudentList);
                    //itemView.getContext().startActivity(new Intent(itemView.getContext(), StudentsActivity.class));
                    //                    Intent intentStudentList = new Intent(itemView.getContext(), StudentListActivity.class);
                    //                    //intentStudentList.putExtra("substrFirstNameStudent", "rov"); // по умолчанию подстрока поиска равна "rov" (Petrov, Sidorov)
                    //                    intentStudentList.putExtra("isUserAdministrator", true);
                    //                    //intentStudentList.putExtra("isUserAdministrator", isUserAdministrator);
                    //                    itemView.getContext().startActivity(intentStudentList);

                }
            });

        }


    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_list_item, parent, false);
        return new RVAdapterGroup.GroupViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        holder.nameGroup.setText(listGroups.get(position).getName());
        holder.numberGroup.setText("" + listGroups.get(position).getNumberGroup());
        // привязка экземпляра объекта студента к строке списка
        //holder.bind(students.get(position));
    }

    @Override
    public long getItemId(int position) {
        return listGroups.get(position).getId();
        //return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return listGroups.size();
    }

    public static Group getchoiseGroup() {
        return choiseGroup;
    }
}
