package com.std.students_andr.students_andr.models.loaders;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.CursorLoader;

import com.std.students_andr.students_andr.models.sqlite.DBHelper;

public class CursorLoaderStudents extends CursorLoader {
    private Cursor mCursor;
    private Context context;


    public CursorLoaderStudents(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public Cursor loadInBackground() {
        // выполнение загрузки студентов из таблицы
        DBHelper helper = new DBHelper(context, "studentdb", null, 1);
        SQLiteDatabase database = helper.getWritableDatabase();
        Cursor cursor = database.query("students", null, null, null, null, null, null);
        //        Student student = null;
        //        while (cursor.moveToNext()) {
        //            student = MainActivity.manager.addStudent2(cursor.getString(cursor.getColumnIndex("firstname"))
        //                    , cursor.getString(cursor.getColumnIndex("surname"))
        //                    , cursor.getString(cursor.getColumnIndex("secondname")));
        //            listSt.add(student);
        //        }
        helper.close();
        return cursor;
    }

    @Override
    public void deliverResult(Cursor cursor) {
        if (isReset()) {
            if (cursor != null) {
                cursor.close();
            }
            return;
        }
        Cursor oldCursor = mCursor;
        mCursor = cursor;

        if (isStarted()) {
            super.deliverResult(cursor);
        }

        if (oldCursor != null && oldCursor != cursor && !oldCursor.isClosed()) {
            oldCursor.close();
        }
    }

    @Override
    protected void onStartLoading() {
        if (mCursor != null) {
            deliverResult(mCursor);
        }
        if (takeContentChanged() || mCursor == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    public void onCanceled(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();

        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
    }
}
