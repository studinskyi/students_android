package com.std.students_andr.students_andr.activities.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.activities.SettingsActivity;
import com.std.students_andr.students_andr.activities.add.AddGroupActivity;
import com.std.students_andr.students_andr.fragments.FragmentGroupList;
import com.std.students_andr.students_andr.models.Group;

import java.util.ArrayList;
import java.util.List;

public class GroupsActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    FragmentGroupList fragmentGroupList;
    Menu menu_groups_list;
    private List<Group> listGroups = new ArrayList<>();
    private String searchSubstr = "";
    //private SearchView miSearchGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        Log.d(MainActivity.getLogTAG(), "onCreate - activity_groups");

        fragmentGroupList = (FragmentGroupList) getFragmentManager().findFragmentById(R.id.fragmentGroupList);
        // получение параметра подстроки фильтрации substrNameGroup из родительского активити
        Intent intent = getIntent();
        searchSubstr = intent.getStringExtra("substrNameGroup");
        //        miSearchGroup = (SearchView) findViewById(R.id.miSearchGroup);
        //        if (!searchSubstr.equals("") && searchSubstr != null)
        //            miSearchGroup.setQuery(searchSubstr, false);
    }

    private void filterListGroupByName(String name) {
        listGroups.clear();
        if (name.equals("") || name == null) {
            // подбор всего списка групп (без фильтрации)
            for (Group group : MainActivity.manager.getGroupsList())
                listGroups.add(group);
        } else {
            // получение группы по наименованию
            for (Group group : MainActivity.manager.getGroupsListFilteredByName(name))
                listGroups.add(group);
            //Group findGroup = MainActivity.manager.getGroupByNameGroup(name);
            //            if (findGroup != null)
            //                listGroups.add(findGroup);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(MainActivity.getLogTAG(), "onResume - activity_groups");
        refreshListInFragment();
    }

    public void refreshListInFragment() {
        Log.d(MainActivity.getLogTAG(), "refreshListInFragment - activity_groups");
        // учет строки фильтра при выводе списка групп
        filterListGroupByName(searchSubstr);
        fragmentGroupList.setListGroups(listGroups);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu_groups_list = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_groups_list, menu);

        MenuItem searchItem = menu.findItem(R.id.miSearchGroup);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAddGroup:
                Log.d(MainActivity.getLogTAG(), "Нажата кнопка AddGroup в activity_groups");
                //Toast.makeText(this, "Нажата кнопка ОК", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getBaseContext(), AddGroupActivity.class));
                return true;
            case R.id.miGroup_settings:
                Log.d(MainActivity.getLogTAG(), "Нажата кнопка Group_settings в activity_groups");
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                //startActivity(new Intent(getBaseContext(), AddGroupActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // User pressed the search button
        Log.d(MainActivity.getLogTAG(), "onQueryTextSubmit - activity_groups");
        searchSubstr = query;
        refreshListInFragment();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // User changed the text
        Log.d(MainActivity.getLogTAG(), "onQueryTextChange - activity_groups");
        searchSubstr = newText;
        refreshListInFragment();
        return false;
    }
}
