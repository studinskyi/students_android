package com.std.students_andr.students_andr.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.element.LessonElementActivity;
import com.std.students_andr.students_andr.models.Lesson;

import java.util.List;

public class RVAdapterLesson extends RecyclerView.Adapter<RVAdapterLesson.LessonViewHolder> {
    private List<Lesson> listLesson;
    private static Lesson choiseLesson;

    public RVAdapterLesson(List<Lesson> listLesson) {
        this.listLesson = listLesson;
    }

    public class LessonViewHolder extends RecyclerView.ViewHolder {
        TextView lessonName;
        TextView lessonStartTime;
        private ActionMode mActionMode;

        public LessonViewHolder(final View itemView) {
            super(itemView);

            lessonName = (TextView) itemView.findViewById(R.id.tvLessonItemName);
            lessonStartTime = (TextView) itemView.findViewById(R.id.tvLessonItemStartTime);

            // 1-й вариант вызова контекстного меню через "Использование режима контекстных действий"
            final ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.menu_sms_call, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                // Called when the user selects a contextual menu item
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    Toast.makeText(itemView.getContext(), "выбран пункт меню " + listLesson.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
                    switch (item.getItemId()) {
                        case R.id.miSendSMS:
                            Intent intentSendSMS = new Intent(Intent.ACTION_SENDTO);
                            intentSendSMS.setData(Uri.parse("smsto:+79124277403"));
                            intentSendSMS.putExtra("sms_body", "text sms");
                            intentSendSMS.resolveActivity(itemView.getContext().getPackageManager());
                            itemView.getContext().startActivity(intentSendSMS);
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        case R.id.miCall:
                            Intent intentCall = new Intent(Intent.ACTION_DIAL);
                            intentCall.setData(Uri.parse("tel:+79102366669"));
                            intentCall.resolveActivity(itemView.getContext().getPackageManager());
                            itemView.getContext().startActivity(intentCall);
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    mActionMode = null;
                }
            };

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                // Called when the user long-clicks on someView
                public boolean onLongClick(View view) {
                    if (mActionMode != null) {
                        return false;
                    }

                    // Start the CAB using the ActionMode.Callback defined above
                    mActionMode = ((Activity) itemView.getContext()).startActionMode(mActionModeCallback);
                    view.setSelected(true);
                    return true;
                }
            });

            // 2-й вариант вызова контекстного меню через переопределение  itemView.setOnCreateContextMenuListener
            //            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            //                @Override
            //                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            //                    MenuItem itemCall = menu.add("call " + listGroups.get(getAdapterPosition()).getName());
            //                    itemCall.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            //                        @Override
            //                        public boolean onMenuItemClick(MenuItem item) {
            //                            Intent myIntent = new Intent(Intent.ACTION_DIAL);
            //                            myIntent.setData(Uri.parse("tel:+79102366669"));
            //                            myIntent.resolveActivity(itemView.getContext().getPackageManager());
            //                            itemView.getContext().startActivity(myIntent);
            //                            return true;
            //                        }
            //                    });
            //                    //Toast.makeText(itemView.getContext(), "выбран пункт меню " + listGroups.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
            //                }
            //            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    choiseLesson = listLesson.get(pos);
                    Intent intentProfileStudent = new Intent(itemView.getContext(), LessonElementActivity.class);
                    //Intent intentProfileStudent = new Intent(itemView.getContext(), ProfileActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentProfileStudent.putExtra("lessonId", choiseLesson.getId()); // передача id урока в карточку элемента урока
                    itemView.getContext().startActivity(intentProfileStudent);
                    //itemView.getContext().startActivity(new Intent(itemView.getContext(), ProfileActivity.class));
                }
            });

        }
    }

    @Override
    public LessonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_list_item, parent, false);
        return new LessonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(LessonViewHolder holder, int position) {
        //holder.bind(mGroupList.get(position));
        holder.lessonName.setText(listLesson.get(position).getName());
        holder.lessonStartTime.setText(listLesson.get(position).getStartTime().getTime().toString());
        // привязка экземпляра объекта студента к строке списка
        //holder.bind(listLesson.get(position));
    }

    @Override
    public long getItemId(int position) {
        return listLesson.get(position).getId();
        //return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return listLesson.size();
    }

    public static Lesson getChoiseLesson() {
        return choiseLesson;
    }
}
