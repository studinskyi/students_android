package com.std.students_andr.students_andr.activities.add;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.managers.Manager;

public class AddGroupActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAddGroup;
    private EditText etNameGroup;

    private Context context;
    private static final String TAG = "myStudents";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        context = this;

        etNameGroup = (EditText) findViewById(R.id.edNameGroup);
        btnAddGroup = (Button) findViewById(R.id.btnAddGroup);
        btnAddGroup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddGroup:
                Log.d(TAG, "Нажата кнопка AddGroup в activity_add_group");
                //Toast.makeText(this, "Нажата кнопка ОК", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(context, GroupListActivity.class));
                String nameGroupNew = etNameGroup.getText().toString();
                Manager manager = MainActivity.manager;
                if (manager.findGroup(nameGroupNew) == null)
                    manager.addGroup(nameGroupNew, 1);

                finish();
                //moveTaskToBack()
                break;
        }
    }
}
