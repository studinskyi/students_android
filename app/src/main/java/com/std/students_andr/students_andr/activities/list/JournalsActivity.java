package com.std.students_andr.students_andr.activities.list;

import android.content.Intent;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.activities.add.AddStudentActivity;
import com.std.students_andr.students_andr.fragments.FragmentJournalList;
import com.std.students_andr.students_andr.fragments.FragmentStudentList;
import com.std.students_andr.students_andr.models.Lesson;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.List;

public class JournalsActivity extends AppCompatActivity {

    FragmentJournalList fragmentJournalList;
    private EditText etJournalFilterFragment;
    private TextView tvCaptionJournals;
    public static Student choiceStudent;
    Menu menu_journal_list;

    String journalFilter = "";
    List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents = new ArrayList<>();

    private static final String TAG = "myStudents";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journals);

        etJournalFilterFragment = (EditText) findViewById(R.id.etJournalFilterFragment);
        tvCaptionJournals = (TextView) findViewById(R.id.tvCaptionJournals);
        // получение параметра подстроки фильтрации из родительского активити
        //Intent intent = getIntent();
        //        etJournalFilterFragment.setText(intent.getStringExtra("substrJournalFilter"));
        //        choiceStudent = MainActivity.manager.getStudentById(intent.getLongExtra("studentId", 0));
        //choiceGroup = MainActivity.manager.getGroupByNameGroup(intent.getStringExtra("nameChoiseGroup"));

        // приобщение события изменения текста (для обновления фильтра при изменение)
        etJournalFilterFragment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // обновление фильтра списка после изменения текста в строке фильтрации
                onResume();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (MainActivity.isUserAdministrator()) {
            tvCaptionJournals.setText("Список журналов");
            //this.menu_student_list.setGroupVisible(,);
        } else {
            tvCaptionJournals.setText("журнал студента " + choiceStudent.getSurname());
        }

        journalFilter = etJournalFilterFragment.getText().toString();
        filterListJournals(journalFilter); // определение общей выборки студентов и фильтрация

        fragmentJournalList = (FragmentJournalList) getFragmentManager().findFragmentById(R.id.fragmentJournalList);
    }

    //    @Override
    //    protected void onNewIntent(Intent intent) {
    //        super.onNewIntent(intent);
    //        // получение параметра подстроки фильтрации из родительского активити
    //        etFirstNameFilterFragment.setText(intent.getStringExtra("substrFirstNameStudent"));
    //        choiceGroup = MainActivity.manager.getGroupByNameGroup(intent.getStringExtra("nameChoiseGroup"));
    //        //Toast.makeText(this,"сработал метод onNewIntent в активити StudentsActivity", Toast.LENGTH_SHORT).show();
    //    }

    private void filterListJournals(String filterSubstr) {
        lessonsPresents.clear();
        lessonsPresents.addAll(MainActivity.manager.getJournalsLessonsPresentsAll());

        //        if (MainActivity.isUserAdministrator() && choiceStudent == null) {
        //            if (filterSubstr.equals("") || filterSubstr == null)
        //                lessonsPresents.addAll(MainActivity.manager.getStudentsListAll());
        //            else
        //                lessonsPresents.addAll(MainActivity.manager.getStudentListFilteredBySurname(filterSubstr));
        //        } else {
        //            // если всё же определен студент, то выборку журналов нужно делать именно по нему
        //            for (Student student : MainActivity.manager.getStudentsListInGroup(choiceStudent)) {
        //                if (!filterSubstr.equals("") && !(filterSubstr == null)) // фильтрация по подстроке filterSubstr
        //                    if (!student.getSurname().contains(filterSubstr)) // строка фильтра filterSubstr задана, но не найдена в элементе
        //                        continue;
        //
        //                lessonsPresents.add(student);
        //            }
        //        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume - activity_groups");
        // учет строки фильтра при выводе списка групп
        journalFilter = etJournalFilterFragment.getText().toString();
        filterListJournals(journalFilter); // определение общей выборки студентов и фильтрация

        fragmentJournalList.setLessonsPresentsList(lessonsPresents);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        this.menu_journal_list = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_journal_list, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAddJournal:
                Log.d(TAG, "Нажата кнопка AddJournal в activity_students");
                //startActivity(new Intent(getBaseContext(), AddStudentActivity.class));
                return true;
            case R.id.miShowAllJournals:
                Log.d(TAG, "Нажата кнопка ShowAllJournals в activity_students");
                if (MainActivity.isUserAdministrator()) {
                    choiceStudent = null;
                    onResume();
                } else {
                    Toast.makeText(this, "Список всех журналов доступен только администраторам!", Toast.LENGTH_SHORT).show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static Student getChoiceStudent() {
        return choiceStudent;
    }

    public static void setChoiceStudent(Student choiceStudent) {
        JournalsActivity.choiceStudent = choiceStudent;
    }
}
