package com.std.students_andr.students_andr.managers;

import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ManagerGroups implements ManagerInterface {
    private Manager manager;
    private HashMap<Long, Group> groups;
    //private List<Group> listGroup; // для промежуточного хранения актуального списка групп (используется при отображении адаптером списков)
    //private List<Student> listStudents; // для промежуточного хранения списка студентов группы (используется при отображении адаптером списков)

    public ManagerGroups(Manager manager) {
        this.manager = manager;
        this.groups = new HashMap<>();
        //this.listGroup = new ArrayList<>();
        //this.listStudents = new ArrayList<>();
    }

    // реализация синглтона менеджера через класс-холдер
    public ManagerGroups() {
        this.groups = new HashMap<>();
    }

    private static class ManagerGroupsHolder {
        private final static ManagerGroups instance = new ManagerGroups();
    }

    public static ManagerGroups getInstance() {
        return ManagerGroupsHolder.instance;
    }

    public Group addGroup(String name, int numberGroup) {
        Group groupNew;
        Group groupFound = findGroup(numberGroup, name);
        if (groupFound == null) {
            groupNew = new Group(name, numberGroup);
            groups.put(groupNew.getId(), groupNew);
            //listGroup.add(groupNew);
        } else
            groupNew = groupFound;

        return groupNew;
    }

    public Group findGroup(int numberGroup) {
        //Group groupFound = null;
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getNumberGroup() == numberGroup)
                return entry.getValue();

        return null; // возвращает null если группа по искомому полю не была найдена
    }

    public Group findGroup(int numberGroup, String name) {
        //Group groupFound = null;
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getName().equals(name) && entry.getValue().getNumberGroup() == numberGroup)
                return entry.getValue();

        return null; // возвращает null если группа по искомому полю не была найдена
    }

    public Group findGroup(String name) {
        //Group groupFound = null;
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getName().equals(name))
                return entry.getValue();

        return null; // возвращает null если группа по искомому полю не была найдена
    }

    public Long getGroupIdByNumberGroup(int numberGroup) {
        //Long idGroup = 0l;
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getNumberGroup() == numberGroup)
                return entry.getKey();

        return (long) 0; // возвращает (long) 0 если группа по искомому полю не была найдена
    }

    public Group getGroupByNameGroup(String name) {
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getName().equals(name))
                return entry.getValue();

        return null; // возвращает null если группа по искомому полю не была найдена
    }

    public Group getGroupByStudent(Student student) {
        return groups.get(student.getGroupID());
    }

    public Group getGroupById(Long groupId) {
        return groups.get(groupId);
    }

    public List<Group> getGroupsListFilteredByName(String name) {
        List<Group> listGr = new ArrayList<>();
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getName().contains(name))
                listGr.add(entry.getValue());
        //List<Group> listGr = new ArrayList<Group>(groups.values());
        String str = "sdfs";

        return listGr;
    }

    public HashMap<Long, Group> getGroups() {
        return groups;
    }

    public List<Group> getGroupsList() {
        //HashMap<Long,Group> groupList = new HashMap<>();
        //        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
        //            groupList.add(entry.getValue());
        List<Group> listGr = new ArrayList<Group>(groups.values());
        return listGr;
    }

    public HashMap<Long, Student> getStudentsInGroup(Group group) {
        //        // возвращение динамического списка студентов из HashMap в группе
        //        HashMap<Long, Student> students = group.getStudents();
        //        List<Student> list = new ArrayList<Student>(students.values());
        return group.getStudents();
    }

    public Student addNewStudentToGroup(Calendar dateOfBirth, String firstName,
                                        String surname, String secondName, Group group) {
        Student studentNew = manager.get_mStudents().addStudent(dateOfBirth, group.getId(), firstName, surname, secondName);
        if (studentNew != null) {
            addStudentToGroup(studentNew, group);
            //            if (!listStudents.contains(studentNew))
            //                listStudents.add(studentNew);  // для промежуточного хранения списка студентов группы
        }
        return studentNew;
    }

    public void addStudentToGroup(Student student, Group group) {
        //        Group groupCurrent = manager.get_mGroups().getGroups().get(student.getGroupID());
        //        if (groupCurrent.equals(group)) // если студент уже находится в этой группе то делать ничего не нужно
        //            return;

        removeStudentFromGroup(student, group); // исключение студента из старой группы
        student.setGroupID(group.getId()); // установка id-группы в поле groupID студента
        group.getStudents().put(student.getId(), student);
        student.setGroupID(group.getId()); // приписка в экземпляре студента в поле groupID ид-шник группы
        //        if (!listStudents.contains(student))
        //            listStudents.add(student); // для промежуточного хранения списка студентов группы
        //        List<Student> studentsInGroup = group.getStudents();
        //        if (!studentsInGroup.contains(student)) {
        //            studentsInGroup.add(student);
        //            successfulAdd = true;
        //        }
    }

    public void removeStudentFromGroup(Student student, Group group) {
        group.getStudents().remove(student.getId());
    }

    public List<Student> getStudentsListInGroup(Group group) {
        //        // для промежуточного хранения списка студентов группы (используется при отображении адаптером списков)
        //        listStudents.clear();
        //
        //        HashMap<Long, Student> students = group.getStudents();
        //        List<Student> list = new ArrayList<Student>(students.values());
        //        // проход по всем студентам и накопление тех, кто учиться в указанной группе
        //        for (Student student : list)
        //            //if (listStudents.contains(student))
        //            listStudents.add(student);
        //        return listStudents;

        HashMap<Long, Student> students = group.getStudents();
        List<Student> listSt = new ArrayList<Student>(students.values());
        return listSt;
    }

    @Override
    // переопределён для использования в Proxy и Mock тестах
    public Object addGroup(Object obj, int numberGroup) {
        return addGroup((String) obj, numberGroup);
    }
}
