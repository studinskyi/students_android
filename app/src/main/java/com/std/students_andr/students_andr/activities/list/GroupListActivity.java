package com.std.students_andr.students_andr.activities.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.activities.add.AddGroupActivity;
import com.std.students_andr.students_andr.models.Group;

import java.util.ArrayList;
import java.util.List;

public class GroupListActivity extends AppCompatActivity {
    private ListView listViewGroups;
    private EditText etNameGroupFilter;
    // переменная объекта menu
    Menu menu_groups_list;

    String nameGroupFilter = "";
    private Context context;
    private ArrayAdapter<Group> arrayAdapterGroups;
    private List<Group> listGroups = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);
        context = this;

        Log.d(MainActivity.getLogTAG(), "onCreate - activity_group_list");

        listViewGroups = (ListView) findViewById(R.id.lvGroupList);
        //        btnAddGroupInList = (Button) findViewById(R.id.btnAddGroupInList);
        //        btnFilterByNameGroup = (Button) findViewById(R.id.btnFilterByNameGroup);
        etNameGroupFilter = (EditText) findViewById(R.id.etNameGroupFilter);

        // получение параметра подстроки фильтрации substrNameGroup из родительского активити
        Intent intent = getIntent();
        etNameGroupFilter.setText(intent.getStringExtra("substrNameGroup"));

        // приобщение события изменения текста (для обновления фильтра при изменение)
        etNameGroupFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onResume();
                //String searchSequence = etNameGroupFilter.getText().toString();
                //startActivity(new Intent(getBaseContext(), GroupListActivity.class)
                //        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP).putExtra("sS", searchSequence));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //        btnAddGroupInList.setOnClickListener(this);
        //        btnFilterByNameGroup.setOnClickListener(this);

        // учет строки фильтра при выводе списка групп
        nameGroupFilter = etNameGroupFilter.getText().toString();
        filterListGroupByName(nameGroupFilter);

        arrayAdapterGroups =
                new ArrayAdapter<Group>(this, android.R.layout.simple_list_item_1, listGroups);

        listViewGroups.setAdapter(arrayAdapterGroups);

        listViewGroups.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //вывод группы в тост
                Group choiseGroup = (Group) parent.getAdapter().getItem(position);
                //Toast.makeText(GroupListActivity.this, choiceGroup.toString(), Toast.LENGTH_SHORT).show();
                StudentsActivity.choiceGroup = choiseGroup;
                startActivity(new Intent(context, StudentsActivity.class));
            }
        });

    }

    private void filterListGroupByName(String name) {
        listGroups.clear();
        if (name.equals("") || name == null) {
            // подбор всего списка групп (без фильтрации)
            for (Group group : MainActivity.manager.getGroupsList())
                listGroups.add(group);
        } else {
            // получение группы по наименованию
            //Group findGroup = MainActivity.manager.getGroupByNameGroup(name);
            for (Group group : MainActivity.manager.getGroupsListFilteredByName(name))
                listGroups.add(group);
            //            if (findGroup != null)
            //                listGroups.add(findGroup);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(MainActivity.getLogTAG(), "onResume - activity_group_list");
        // учет строки фильтра при выводе списка групп
        nameGroupFilter = etNameGroupFilter.getText().toString();
        filterListGroupByName(nameGroupFilter);

        arrayAdapterGroups.notifyDataSetChanged();
    }

    //    @Override
    //    public void onClick(View v) {
    //        switch (v.getId()) {
    //            case R.id.btnAddGroupInList:
    //                Log.d(MainActivity.getLogTAG(), "Нажата кнопка AddGroup в activity_group_list");
    //                //Toast.makeText(this, "Нажата кнопка ОК", Toast.LENGTH_SHORT).show();
    //                startActivity(new Intent(context, AddGroupActivity.class));
    //                break;
    //            case R.id.btnFilterByNameGroup:
    //                // запускает Activity в новом таске. Если уже существует таск с экземпляром данной Activity, то этот таск становится активным, и срабатываем метод onNewIntent().
    //                //startActivity(new Intent(context, GroupListActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    //
    //                // если Activity запускает сама себя, т.е. она находится в вершине стека, то вместо создания нового экземпляра в стеке вызывается метод onNewIntent().
    //                // Флаг аналогичен параметру singleTop описанному выше
    //                //startActivity(new Intent(context, GroupListActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP ));
    //
    //                onResume();
    //                break;
    //        }
    //    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putString("substrNameGroup", etNameGroupFilter.getText().toString());
        Log.d(MainActivity.getLogTAG(), "onSaveInstanceState - activity_group_list");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //etNameGroupFilter.setText(savedInstanceState.getString("substrNameGroup"));
        Log.d(MainActivity.getLogTAG(), "onRestoreInstanceState - activity_group_list");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(MainActivity.getLogTAG(), "onStop - activity_group_list");
        //onSaveInstanceState(getIntent().getExtras());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(MainActivity.getLogTAG(), "onDestroy - activity_group_list");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(MainActivity.getLogTAG(), "onStart - activity_group_list");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(MainActivity.getLogTAG(), "onNewIntent - activity_group_list");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(MainActivity.getLogTAG(), "onPause - activity_group_list");
    }

    // создание меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        this.menu_groups_list = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_groups_list, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAddGroup:
                Log.d(MainActivity.getLogTAG(), "Нажата кнопка AddGroup в activity_group_list");
                //Toast.makeText(this, "Нажата кнопка ОК", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, AddGroupActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
