package com.std.students_andr.students_andr.activities.list;

import android.content.Context;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.adapters.RVAdapterJournal;
import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Journal;
import com.std.students_andr.students_andr.models.Lesson;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class JournalListActivity extends AppCompatActivity {
    private RecyclerView rvJournalList; // для варианта RecyclerView
    private Context context;
    private TextView tvCaptionJournalList;
    public static Student choiceStudent;

    RVAdapterJournal adapter_RV;
    //List<Pair<Lesson,Boolean>> lessonsPresents = new ArrayList<>();
    HashMap<Lesson, Boolean> presentStudent = new HashMap<>();

    private static final String TAG = "myStudents";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal_list);

        context = this;

        tvCaptionJournalList = (TextView) findViewById(R.id.tvCaptionJournalList);
        tvCaptionJournalList.setText("посещаемость студентов ");
        //tvCaptionJournalList.setText("посещаемость студента " + choiceStudent.getSurname());

        rvJournalList = (RecyclerView) findViewById(R.id.rvJournalList);

        RecyclerView rvJournalList = (RecyclerView) findViewById(R.id.rvJournalList);
        rvJournalList.setHasFixedSize(true); //  размер RecyclerView не будет изменяться (для улучшения производительности)
        LinearLayoutManager linLM = new LinearLayoutManager(context);
        rvJournalList.setLayoutManager(linLM);

        //        Group gr1 = MainActivity.manager.addGroup("группа 1", 1);
        //        Group gr2 = MainActivity.manager.addGroup("группа 2", 2);
        //        Group gr3 = MainActivity.manager.addGroup("группа 3", 3);
        //        Lesson les1 = MainActivity.manager.addLesson("История", new GregorianCalendar(2017, Calendar.JUNE, 10), 60, "кабинет 101", "История древних времен", "Древняя Русь", "Сидоров И.П.", gr1);
        //        Lesson les2 = MainActivity.manager.addLesson("Философия", new GregorianCalendar(2017, Calendar.AUGUST, 16), 60, "кабинет 202", "Труды немецких философов", "Философия капитала", "Карл Маркс", gr2);
        //        Lesson les3 = MainActivity.manager.addLesson("Филология", new GregorianCalendar(2017, Calendar.SEPTEMBER, 7), 60, "кабинет 303", "Романо-германская филология", "Зарубежная филология", "Газизов Рафаэль", gr3);
        //        Lesson les4 = MainActivity.manager.addLesson("Android", new GregorianCalendar(2017, Calendar.JULY, 5), 60, "кабинет 501", "Android Activity", "Android Views", "Артем Первушов", gr3);
        //
        //        Student st1 = MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1994, Calendar.FEBRUARY, 8), "Petr", "Petrov", "Petrovich", gr1);
        //        Student st2 = MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1980, Calendar.NOVEMBER, 25), "Ivan", "Ivanov", "Ivanovich", gr2);
        //        Student st3 = MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1997, Calendar.MAY, 16), "Alexandr", "Alexandrov", "Alexandrovich", gr3);
        //        Student st4 = MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1975, Calendar.APRIL, 14), "Viktor", "Viktorov", "Viktorovich", gr3);
        //
        //        HashMap<Student, HashMap<Lesson, Boolean>> presentHashAll = new HashMap<>();
        //        presentStudent = new HashMap<>();
        //        presentStudent.put(les1, true);
        //        presentStudent.put(les2, false);
        //        presentStudent.put(les3, true);
        //        presentStudent.put(les4, true);
        //        presentHashAll.put(st1, presentStudent);
        //        presentStudent = new HashMap<>();
        //        presentStudent.put(les1, true);
        //        presentStudent.put(les2, false);
        //        presentStudent.put(les3, true);
        //        presentStudent.put(les4, true);
        //        presentHashAll.put(st2, presentStudent);
        //        presentStudent = new HashMap<>();
        //        presentStudent.put(les1, true);
        //        presentStudent.put(les2, false);
        //        presentStudent.put(les3, true);
        //        presentStudent.put(les4, true);
        //        presentHashAll.put(st3, presentStudent);
        //
        //        // трансформация исходного HashMap<Student, HashMap<Lesson,Boolean>>
        //        // в список вида List<Pair<Pair<Student,Lesson>, Boolean> для передачи в адаптер
        //        List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents = new ArrayList<>();
        //        for (HashMap.Entry<Student, HashMap<Lesson, Boolean>> entry : presentHashAll.entrySet())
        //            for (HashMap.Entry<Lesson, Boolean> entryLes : entry.getValue().entrySet()) {
        //                Pair<Student, Lesson> pairStLes = new Pair<>(entry.getKey(), entryLes.getKey());
        //                Pair<Pair<Student, Lesson>, Boolean> pairPres = new Pair<>(pairStLes, entryLes.getValue());
        //                lessonsPresents.add(pairPres);
        //            }

        //        List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents = new ArrayList<>();
        //        List<Journal> journals = MainActivity.manager.getJournalsListAll();
        //        for (Journal journal : journals)
        //            for (HashMap.Entry<Student, HashMap<Lesson, Boolean>> entry : journal.getPresentStudents().entrySet())
        //                for (HashMap.Entry<Lesson, Boolean> entryLes : entry.getValue().entrySet()) {
        //                    Pair<Student, Lesson> pairStLes = new Pair<>(entry.getKey(), entryLes.getKey());
        //                    Pair<Pair<Student, Lesson>, Boolean> pairPres = new Pair<>(pairStLes, entryLes.getValue());
        //                    lessonsPresents.add(pairPres);
        //                }
        List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents = MainActivity.manager.getJournalsLessonsPresentsAll();

        //entry.getKey() - студент

        adapter_RV = new RVAdapterJournal(lessonsPresents);
        //adapter_RV = new RVAdapterJournal(presentStudent);
        rvJournalList.setAdapter(adapter_RV);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        //        listStudents.clear();
        //        for (Student student : MainActivity.manager.getStudentsListInGroup(choiceGroup))
        //            listStudents.add(student);

        adapter_RV.notifyDataSetChanged();
    }
}
