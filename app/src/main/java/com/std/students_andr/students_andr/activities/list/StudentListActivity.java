package com.std.students_andr.students_andr.activities.list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.activities.add.AddStudentActivity;
import com.std.students_andr.students_andr.adapters.RVAdapterStudent;
import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Student;
import com.std.students_andr.students_andr.models.loaders.ReadStudentsSQLAsyncTask;
import com.std.students_andr.students_andr.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class StudentListActivity extends Activity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {
//public class StudentListActivity extends AppCompatActivity {

    private RecyclerView rvStudentList; // для варианта RecyclerView
    private Context context;
    private Button btnAddStudentInList;
    private Button btnFilterByFirsName;

    private TextView tvCaptionStudentsList;
    private EditText etFirsNameFilter;
    RVAdapterStudent adapter_RV;
    public static Group choiceGroup;
    private static List<Student> listStudents = new ArrayList<>();

    private String substrFirstNameStudent;
    //private CursorLoaderStudents cursorLoaderStudents;
    ReadStudentsSQLAsyncTask task;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);
        context = this;

        tvCaptionStudentsList = (TextView) findViewById(R.id.tvCaptionStudentsList);
        rvStudentList = (RecyclerView) findViewById(R.id.rvStudentList);
        btnAddStudentInList = (Button) findViewById(R.id.btnAddStudentInList);
        btnFilterByFirsName = (Button) findViewById(R.id.btnFilterByFirsName);
        btnAddStudentInList.setOnClickListener(this);
        btnFilterByFirsName.setOnClickListener(this);
        etFirsNameFilter = (EditText) findViewById(R.id.etFirsNameFilter);

        // получение параметра подстроки фильтрации и флага админ. прав пользователя из родительского активити
        Intent intent = getIntent();
        substrFirstNameStudent = intent.getStringExtra("substrFirstNameStudent");
        //isUserAdministrator = intent.getBooleanExtra("isUserAdministrator", false);
        etFirsNameFilter.setText(substrFirstNameStudent);

        // приобщение события изменения текста (для обновления фильтра при изменение)
        etFirsNameFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onResume();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (MainActivity.isUserAdministrator()) {
            choiceGroup = null;
            tvCaptionStudentsList.setText("Список студентов из групп");
            btnAddStudentInList.setEnabled(false); // запрет добавления студентов в общем списке (это делать нужно через открытие групп)

        } else {
            tvCaptionStudentsList.setText("группа " + choiceGroup.getName());
        }

        //students = MainActivity.manager.getStudentsListInGroup(choiceGroup);
        //initializeData();
        RecyclerView rvStudentList = (RecyclerView) findViewById(R.id.rvStudentList);
        rvStudentList.setHasFixedSize(true); //  размер RecyclerView не будет изменяться (для улучшения производительности)
        LinearLayoutManager linLM = new LinearLayoutManager(context);
        rvStudentList.setLayoutManager(linLM);

        //        //Адаптер для ListView
        //        cursorLoaderStudents = new SimpleCursorAdapter(this,
        //                android.R.layout.simple_list_item_2, null,
        //                new String[]{Contacts.DISPLAY_NAME, Contacts.CONTACT_STATUS},
        //                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        //
        //        setListAdapter(adapter);

        //filterListStudents();
        startAsyncTask_readStudentsSQL("", etFirsNameFilter.getText().toString());

        adapter_RV = new RVAdapterStudent(listStudents);
        rvStudentList.setAdapter(adapter_RV);
    }

    private void filterListStudents() {
        listStudents.clear();
        if (MainActivity.isUserAdministrator())
            for (Student student : MainActivity.manager.getStudentsListAllFromDB())
                listStudents.add(student);
        else
            for (Student student : MainActivity.manager.getStudentsListInGroup(choiceGroup))
                listStudents.add(student);

        substrFirstNameStudent = etFirsNameFilter.getText().toString();
        if (!substrFirstNameStudent.equals("") && substrFirstNameStudent != null) {
            // фильтрация уже имеющегося списка
            List<Student> listFiltr = new ArrayList<>();
            for (Student student : listStudents)
                if (student.getFirstName().contains(substrFirstNameStudent))
                    listFiltr.add(student);

            listStudents.clear();
            for (Student student : listFiltr)
                listStudents.add(student);
        }
    }

    private void startAsyncTask_readStudentsSQL(String nameGroup, String substrFirstNameStudent) {
        task = (ReadStudentsSQLAsyncTask) getLastNonConfigurationInstance();
        if (task == null)
            task = new ReadStudentsSQLAsyncTask(this);
        else
            task.setContext(this);

        task.execute(nameGroup, substrFirstNameStudent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //filterListStudents();
        startAsyncTask_readStudentsSQL("", etFirsNameFilter.getText().toString());
        adapter_RV.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddStudentInList:
                Log.d(MainActivity.getLogTAG(), "Нажата кнопка AddStudentInList в activity_student_list");
                //Toast.makeText(this, "Нажата кнопка ОК", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, AddStudentActivity.class));
                break;
            case R.id.btnFilterByFirsName:
                onResume();
                break;

        }

    }

    private static void initializeData() {
        //        // создание объектов групп
        //        Group gr1 = MainActivity.manager.addGroup("группа 1", 1);
        //        Group gr2 = MainActivity.manager.addGroup("группа 2", 2);
        //        Group gr3 = MainActivity.manager.addGroup("группа 3", 3);

        // добавление студентов
        MainActivity.manager.addNewStudentToGroup(Utils.calend(1994, Calendar.FEBRUARY, 8), "Petr", "Petrov", "Petrovich", choiceGroup);
        MainActivity.manager.addNewStudentToGroup(Utils.calend(1980, Calendar.NOVEMBER, 25), "Ivan", "Ivanov", "Ivanovich", choiceGroup);
        MainActivity.manager.addNewStudentToGroup(Utils.calend(1997, Calendar.MAY, 16), "Alexandr", "Alexandrov", "Alexandrovich", choiceGroup);
        MainActivity.manager.addNewStudentToGroup(Utils.calend(1975, Calendar.APRIL, 14), "Viktor", "Viktorov", "Viktorovich", choiceGroup);
        // формирование списка студентов
        //students = new ArrayList<>();
        //for (HashMap.Entry<Long, Group> entry : MainActivity.manager.getGroups().entrySet())
        //            for (HashMap.Entry<Long, Student> entrySt : entry.getValue().getStudents().entrySet())
        //                students.add(entrySt.getValue());
    }

    public static void setListStudents(List<Student> listSt) {
        StudentListActivity.listStudents.clear();
        StudentListActivity.listStudents.addAll(listSt);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
