package com.std.students_andr.students_andr.activities.add;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.activities.list.StudentsActivity;
import com.std.students_andr.students_andr.models.Group;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class AddStudentActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAddStudent;
    private EditText etFirstNameStudent;
    private EditText etSurnameStudent;
    private EditText etSecondNameStudent;
    private EditText etGroupNameOfStudent;
    private Group choiceGroup;

    private Context context;
    private static final String TAG = "myStudents";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        context = this;

        etFirstNameStudent = (EditText) findViewById(R.id.etFirstNameStudent);
        etSurnameStudent = (EditText) findViewById(R.id.etSurnameStudent);
        etSecondNameStudent = (EditText) findViewById(R.id.etSecondNameStudent);
        etGroupNameOfStudent = (EditText) findViewById(R.id.etGroupNameOfStudent);
        btnAddStudent = (Button) findViewById(R.id.btnAddStudent);
        btnAddStudent.setOnClickListener(this);
        choiceGroup = StudentsActivity.choiceGroup;
        // отражение группы студента
        if (choiceGroup != null)
            etGroupNameOfStudent.setText(choiceGroup.getName());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddStudent:
                //                //startActivity(new Intent(context, GroupListActivity.class));
                //                String nameGroupNew = edNameGroup.getText().toString();
                //                Manager manager = MainActivity.manager;
                //                if(manager.findGroup(nameGroupNew) == null)
                //                    manager.addGroup(nameGroupNew,1);
                String firstNamepNew = etFirstNameStudent.getText().toString();
                String surnameNamepNew = etSurnameStudent.getText().toString();
                String secondNameNew = etSecondNameStudent.getText().toString();
                // проверка наличия группы по введенной строке наименования группы (в случае когда ссылка на группу равна null)
                if (choiceGroup == null)
                    choiceGroup = MainActivity.manager.getGroupByNameGroup(etGroupNameOfStudent.getText().toString());

                // если текущая группа, в котрую будет записан студент не определена, то не давать записать новый экземпляр студента
                if (choiceGroup == null) {
                    Toast.makeText(this, "Укажите наименование существующей группы! Без этого записать студента нельзя.", Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    MainActivity.manager.addNewStudentToGroup(new GregorianCalendar(1994, Calendar.FEBRUARY, 8),
                            firstNamepNew, surnameNamepNew, secondNameNew, choiceGroup);
                    finish();
                    //moveTaskToBack()
                }
                break;
        }
    }
}
