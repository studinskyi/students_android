package com.std.students_andr.students_andr.models;

import java.util.UUID;

public class Contact {
    private Long id;
    private UUID SerialVersionUUID;
    private ContactType type;
    private String value;

    public Contact(ContactType type, String value) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.SerialVersionUUID = UUID.randomUUID();
        this.value = value;
        this.type = type;
    }

    @Override
    public int hashCode() {
        return (21+value.hashCode()*41) + (21+type.hashCode()*41);
    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj);
        if (obj == null)
            return false;

        if (!(obj instanceof Contact))
            return false;

        if (this.value != ((Contact) obj).getValue()) {
            // проверка идентичности типа контакта по полю value
            return false;
        }

        if (this.type != ((Contact) obj).getType()) {
            // проверка идентичности типа контакта по полю type
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "id = " + id +
                "; SerialVersionUUID = " + SerialVersionUUID +
                "; type = " + type +
                "; value = " + value;
    }

    public UUID getSerialVersionUUID() {
        return SerialVersionUUID;
    }

    public Long getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }
}
