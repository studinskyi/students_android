package com.std.students_andr.students_andr.managers;

import android.support.v4.util.Pair;

import com.std.students_andr.students_andr.models.Journal;
import com.std.students_andr.students_andr.models.Lesson;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ManagerJournals {
    private Manager manager;
    private HashMap<Long, Journal> journals;

    public ManagerJournals(Manager manager) {
        this.manager = manager;
        this.journals = new HashMap<>();
    }

    // реализация синглтона менеджера через класс-холдер
    public ManagerJournals() {
        this.journals = new HashMap<>();
    }

    private static class ManagerJournalsHolder {
        private final static ManagerJournals instance = new ManagerJournals();
    }

    public static ManagerJournals getInstance() {
        return ManagerJournalsHolder.instance;
    }

    public Journal addJournal(String name) {
        Journal journalNew;
        Journal journalFound = findJournal(name);
        if (journalFound == null) {
            journalNew = new Journal(name);
            journals.put(journalNew.getId(), journalNew);
        } else
            journalNew = journalFound;

        return journalNew;
    }

    public Journal addJournal(String name, Lesson lesson, HashMap<Long, Student> students) {
        Journal journalNew;
        Journal journalFound = findJournal(name);
        if (journalFound == null) {
            journalNew = new Journal(name);
            addStudentsToJournal(journalNew, students);
            journals.put(journalNew.getId(), journalNew);
        } else
            journalNew = journalFound;

        addStudentsToJournal(journalNew, students); // добавление списка студентов в новый журнал

        return journalNew;
    }

    public Journal findJournal(String namejournal) {
        for (HashMap.Entry<Long, Journal> entry : journals.entrySet())
            if (entry.getValue().getName().equals(namejournal))
                return entry.getValue();

        return null;
    }

    // добавление одного конкретно взятого студента в журнал
    public boolean addStudentToJournal(Journal journal, Student student) {
        boolean successfulAdd = false; // был ли добавлен элемент
//        HashMap<Student, Boolean> studentsInJournal = journal.getPresentStudents();
//        if (!studentsInJournal.containsKey(student)) {
//            studentsInJournal.put(student, false);
//            successfulAdd = true;
//        }

        return successfulAdd;
    }

    // добавление списка студентов в журнал (сюда же входит вариант добавления списка студентов группы)
    public HashMap<Student, Boolean> addStudentsToJournal(Journal journal, HashMap<Long, Student> students) {
        //        HashMap<Student, Boolean> studentsInJournal = journal.getPresentStudents();
        //        if (students.size() > 0) {
        //            for (HashMap.Entry<Long, Student> entry : students.entrySet())
        //                addStudentToJournal(journal, entry.getValue());
        //                //            for (Student student : listStudents)
        //                //                addStudentToJournal(journal, student);
        //        }
        //        return studentsInJournal; // возврат результирующего HashMap присутствия студентов
        return null;
    }

    public Journal getJournalById(Long journalId) {
        return journals.get(journalId);
    }

    public Manager getManager() {
        return manager;
    }

    public List<Journal> getJournalsListAll() {
        List<Journal> listJourn = new ArrayList<>(journals.values());
        return listJourn;
    }

    public List<Pair<Pair<Student, Lesson>, Boolean>> getJournalsLessonsPresentsAll() {
        List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents = new ArrayList<>();
        List<Journal> journals = getJournalsListAll();
        for (Journal journal : journals)
            for (HashMap.Entry<Student, HashMap<Lesson, Boolean>> entry : journal.getPresentStudents().entrySet())
                for (HashMap.Entry<Lesson, Boolean> entryLes : entry.getValue().entrySet()) {
                    Pair<Student, Lesson> pairStLes = new Pair<>(entry.getKey(), entryLes.getKey());
                    Pair<Pair<Student, Lesson>, Boolean> pairPres = new Pair<>(pairStLes, entryLes.getValue());
                    lessonsPresents.add(pairPres);
                }
        //        List<Journal> listJourn = new ArrayList<>(journals.values());
        //        return listJourn;
        return lessonsPresents;
    }

    public HashMap<Long, Journal> getJournals() {
        return journals;
    }
}
