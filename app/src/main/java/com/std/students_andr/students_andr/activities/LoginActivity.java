package com.std.students_andr.students_andr.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.models.User;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    //@BindView(R.id.btnOk)
    private Button btnOk;
    private Button btnRegistration;
    private EditText etLogin;
    private EditText etPassword;
    private Context context;

    private static final String TAG = "myStudents";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //MainActivity.getUser();
        context = this;

        // найдем View-элементы
        //btnOk = (Button) findViewById(R.id.btnOk);
        btnRegistration = (Button) findViewById(R.id.btnRegistration);
        etLogin = (EditText) findViewById(R.id.editLogin);
        etPassword = (EditText) findViewById(R.id.editPassword);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        if (sharedPreferences.getBoolean("save_login", false)) {
            etLogin.setText(sharedPreferences.getString("user_login", ""));
        }
        //        if (sharedPreferences.getBoolean("save_password", false)) {
        //            etPassword.setText(sharedPreferences.getString("user_password", ""));
        //        }

        // присваиваем обработчик кнопкам
        btnOk.setOnClickListener(this);
        btnRegistration.setOnClickListener(this);

        //        // Получаем сообщение из объекта intent
        //        Intent intent = getIntent();
        //        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "onResume() в активити activity_login", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        // по id определяем кнопку, вызвавшую этот обработчик
        switch (v.getId()) {
            case R.id.btnOk:
                // кнопка ОК
                Log.d(TAG, "Нажата кнопка ОК в активити activity_login");
                //Toast.makeText(this, "Нажата кнопка ОК", Toast.LENGTH_SHORT).show();
                //tvOut.setText("Нажата кнопка ОК");
                String loginStr = etLogin.getText().toString();
                String passwStr = etPassword.getText().toString();
                Log.d(TAG, "проверка возможности логина пользователя login: " + loginStr + " password: " + passwStr);
                if (MainActivity.getUserByLogin(loginStr) == null) {
                    Log.d(TAG, "неудачная попытка входа под логином: " + loginStr + " (пользователя с таким логином не существует)");
                    Toast.makeText(this, "Пользователь с логином " + loginStr + " не зарегистрирован!", Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    if (MainActivity.getUserByLogin(loginStr).getPassword().equals(passwStr)) {
                        // установка текущей информации об активном пользователе
                        MainActivity.setUserCurrent(MainActivity.getUserByLogin(loginStr));
                        MainActivity.setLoginCurrent(loginStr);
                        MainActivity.setPasswordCurrent(passwStr);
                        Log.d(TAG, "удачный вход login: " + loginStr + " password: " + passwStr);
                        Toast.makeText(this, "удачный вход login: " + loginStr + " password: " + passwStr, Toast.LENGTH_SHORT).show();
                        if (MainActivity.isUserAdministrator()) {
                            // далее для пользователя-администратора открывается главная страница,
                            Intent intentStartAdmin = new Intent(context, MainActivity.class);
                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                            if (sharedPreferences.getBoolean("save_login", false)) {
                                sharedPreferences.edit().putString("user_login", MainActivity.getUserCurrent().getLogin());
                            }
                            if (sharedPreferences.getBoolean("save_password", false)) {
                                sharedPreferences.edit().putString("user_password", MainActivity.getUserCurrent().getPassword());
                            }
                            startActivity(intentStartAdmin);
                            //startActivity(new Intent(context, MainActivity.class));
                        } else {
                            // а для обычного пользователя открывается профиль соответствуюего ему студента


                        }

                    } else {
                        // сброс всей информаци об активном пользователе (по причине неверного ввода пароля и логина)
                        MainActivity.setUserCurrent(null);
                        MainActivity.setLoginCurrent("");
                        MainActivity.setPasswordCurrent("");
                        Log.d(TAG, "Введен неверный пароль!");
                        Toast.makeText(this, "Введен неверный пароль!", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                //startActivity(new Intent(context, MainActivity.class));
                break;
            case R.id.btnRegistration:
                // кнопка Cancel
                Log.d(TAG, "Нажата кнопка Registration в активити activity_login");
                //tvOut.setText("Нажата кнопка Registration");
                //Toast toast = Toast.makeText(this, "нажата кнопка Registration", Toast.LENGTH_SHORT);
                //toast.setGravity(Gravity.LEFT, 0, 20);
                //toast.show();
                startActivity(new Intent(context, RegistrationActivity.class));
                break;
        }

    }

    public static User login(String loginStr, String passwStr, Activity activity) {
        User userFindByLogin = MainActivity.getUserByLogin(loginStr);
        if (userFindByLogin != null) {
            if (userFindByLogin.getPassword().equals(passwStr)) {
                // установка текущей информации об активном пользователе
                MainActivity.setUserCurrent(userFindByLogin);
                MainActivity.setLoginCurrent(loginStr);
                MainActivity.setPasswordCurrent(passwStr);
                Log.d(TAG, "удачный вход login: " + loginStr + " password: " + passwStr);
                Toast.makeText(activity.getBaseContext(), "удачный вход login: " + loginStr + " password: " + passwStr, Toast.LENGTH_SHORT).show();
            } else {
                // сброс всей информаци об активном пользователе (по причине неверного ввода пароля и логина)
                userFindByLogin = null; //
                MainActivity.setUserCurrent(null);
                MainActivity.setLoginCurrent("");
                MainActivity.setPasswordCurrent("");
                Log.d(TAG, "Введен неверный пароль!");
                Toast.makeText(activity.getBaseContext(), "Введен неверный пароль!", Toast.LENGTH_SHORT).show();
                //break;
            }
        } else {
            Log.d(TAG, "неудачная попытка входа под логином: " + loginStr + " (пользователя с таким логином не существует)");
            Toast.makeText(activity.getBaseContext(), "Пользователь с логином " + loginStr + " не зарегистрирован!", Toast.LENGTH_SHORT).show();
        }

        return userFindByLogin;
    }
}