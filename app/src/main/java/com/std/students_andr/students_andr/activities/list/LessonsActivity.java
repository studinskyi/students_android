package com.std.students_andr.students_andr.activities.list;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.activities.add.AddLessonActivity;
import com.std.students_andr.students_andr.fragments.FragmentLessonList;
import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Lesson;

import java.util.ArrayList;
import java.util.List;

public class LessonsActivity extends AppCompatActivity {

    FragmentLessonList fragmentLessonList;
    private EditText etNameLessonFilterFragment;
    private TextView tvCaptionLessons;
    public static Group choiceGroup;
    Menu menu_lesson_list;

    String nameLessonFilter = "";
    private List<Lesson> listLessons = new ArrayList<>();

    private static final String TAG = "myStudents";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);

        etNameLessonFilterFragment = (EditText) findViewById(R.id.etNameLessonFilterFragment);
        tvCaptionLessons = (TextView) findViewById(R.id.tvCaptionLessons);
        // получение параметра подстроки фильтрации из родительского активити
        Intent intent = getIntent();
        etNameLessonFilterFragment.setText(intent.getStringExtra("substrNameLesson"));
        choiceGroup = MainActivity.manager.getGroupByNameGroup(intent.getStringExtra("nameChoiseGroup"));

        // приобщение события изменения текста (для обновления фильтра при изменение)
        etNameLessonFilterFragment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // обновление фильтра списка после изменения текста в строке фильтрации
                onResume();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (MainActivity.isUserAdministrator()) {
            tvCaptionLessons.setText("Список всех занятий");
            //this.menu_student_list.setGroupVisible(,);
        } else {
            tvCaptionLessons.setText("Список занятий группы " + choiceGroup.getName());
        }

        nameLessonFilter = etNameLessonFilterFragment.getText().toString();
        filterListLessons(nameLessonFilter); // определение общей выборки занятий и фильтрация

        fragmentLessonList = (FragmentLessonList) getFragmentManager().findFragmentById(R.id.fragmentLessonList);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // получение параметра подстроки фильтрации из родительского активити
        etNameLessonFilterFragment.setText(intent.getStringExtra("substrNameLesson"));
        choiceGroup = MainActivity.manager.getGroupByNameGroup(intent.getStringExtra("nameChoiseGroup"));
    }

    private void filterListLessons(String nameLessonSubstr) {
        listLessons.clear();
        if (MainActivity.isUserAdministrator() && choiceGroup == null) {
            if (nameLessonSubstr.equals("") || nameLessonSubstr == null)
                listLessons.addAll(MainActivity.manager.getLessonsListAll());
            else
                listLessons.addAll(MainActivity.manager.getLessonListFilteredByName(nameLessonSubstr));
        } else {
            // если всё же определена группа, то выборку занятий нужно делать именно по ней
            for (Lesson lesson : MainActivity.manager.getLessonListForGroup(choiceGroup)) {
                if (!nameLessonSubstr.equals("") && !(nameLessonSubstr == null)) // фильтрация по подстроке nameLessonSubstr
                    if (!lesson.getName().contains(nameLessonSubstr)) // строка фильтра nameLessonSubstr задана, но не найдена в элементе
                        continue;

                listLessons.add(lesson);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume - activity_lessons");
        // учет строки фильтра при выводе списка
        nameLessonFilter = etNameLessonFilterFragment.getText().toString();
        filterListLessons(nameLessonFilter); // определение общей выборки занятий и фильтрация

        fragmentLessonList.setListLessons(listLessons);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        this.menu_lesson_list = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_lesson_list, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAddLesson:
                Log.d(TAG, "Нажата кнопка AddLesson в activity_lessons");
                startActivity(new Intent(getBaseContext(), AddLessonActivity.class));
                return true;
            case R.id.miShowAllLessons:
                Log.d(TAG, "Нажата кнопка ShowAllLessons в activity_lessons");
                if (MainActivity.isUserAdministrator()) {
                    choiceGroup = null;
                    onResume();
                } else {
                    Toast.makeText(this, "Список всех занятий доступен только администраторам!", Toast.LENGTH_SHORT).show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public List<Lesson> getListLessons() {
        return listLessons;
    }

    public void setListLessons(List<Lesson> listLessons) {
        this.listLessons = listLessons;
    }
}
