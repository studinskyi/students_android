package com.std.students_andr.students_andr.models;

import java.util.UUID;

public class User {
    private Long id;
    private UUID SerialVersionUUID;
    private String login;
    private String password;
    private boolean isAdministrator;

    public User(String login, String password) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.SerialVersionUUID = UUID.randomUUID();
        this.login = login;
        this.password = password;
        this.isAdministrator = false;
    }

    public User(String login, String password, boolean isAdministrator) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.SerialVersionUUID = UUID.randomUUID();
        this.login = login;
        this.password = password;
        this.isAdministrator = isAdministrator;
    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj);
        if (obj == null)
            return false;

        if (!(obj instanceof User))
            return false;

        if (this.id != ((User) obj).getId()) {
            // проверка идентичности студента по полю id
            return false;
        }

        return true;
    }

    public boolean isAdministrator() {
        return isAdministrator;
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 41);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public UUID getSerialVersionUUID() {
        return SerialVersionUUID;
    }
}
