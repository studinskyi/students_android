package com.std.students_andr.students_andr.models;

public enum ContactType {
    PHONE,
    EMAIL,
    TELEGRAM,
    SKYPE,
    VK,
    FACEBOOK,
    LINKEDIN,
    ODNOKLASNIKI,
}
