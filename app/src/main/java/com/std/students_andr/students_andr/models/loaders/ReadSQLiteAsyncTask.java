package com.std.students_andr.students_andr.models.loaders;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class ReadSQLiteAsyncTask extends AsyncTask<String, Integer, Double> {
    // где переопределяется абстрактный класс
    // android.os.AsyncTask<Params,Progress,Result>
    // String - соответствует блоку входны параметров задачи Params и передаётся в метод doInBackground(String... strings)
    // Integer - соответствует блоку Progress и передаётся в метод onProgressUpdate(Integer... values)
    // Double - соответствует блоку Result и передаётся в метод
    Context context;

    public ReadSQLiteAsyncTask(Context context) {
        this.context = context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (context != null) {
            Toast.makeText(context, "onPreExecute", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPostExecute(Double aDouble) {
        // Double - соответствует блоку Result и передаётся в метод
        super.onPostExecute(aDouble);
        Toast.makeText(context, "Result is: " + aDouble, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // Integer - соответствует блоку Progress
        super.onProgressUpdate(values);
        if (context != null) {
            Toast.makeText(context, "onProgressUpdate: " + values[0], Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected Double doInBackground(String... strings) {
        // String - соответствует блоку входны параметров задачи Params
        Integer from = Integer.parseInt(strings[0]);
        Integer to = Integer.parseInt(strings[1]);
        Double sum = 0d;

        for (int i = from; i < to; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishProgress(i);
            sum += i;
        }
        return sum;
    }


}
