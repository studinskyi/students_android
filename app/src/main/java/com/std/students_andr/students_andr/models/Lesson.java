package com.std.students_andr.students_andr.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

public class Lesson {
    private Long id;
    private UUID SerialVersionUUID;
    private String name;
    private Calendar startTime;
    private Integer duration;
    private String room;
    private String description;
    private String subject;
    private String teacher;
    private HashMap<Long, Group> groups;

    public Lesson(String name, Calendar startTime, Integer duration,
                  String room, String description, String subject,
                  String teacher, Group group) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.SerialVersionUUID = UUID.randomUUID();
        this.name = name;
        this.startTime = startTime;
        this.duration = duration;
        this.room = room;
        this.description = description;
        this.subject = subject;
        this.teacher = teacher;
        this.groups = new HashMap<>();
        // добавление группы на занятие
        //if (group != null && groups.get(group.getId()) == null)
        groups.put(group.getId(), group);
    }

    public Lesson(String name, Calendar startTime, Integer duration,
                  String room, String description, String subject) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.SerialVersionUUID = UUID.randomUUID();
        this.name = name;
        this.startTime = startTime;
        this.duration = duration;
        this.room = room;
        // остальные поля объекта Lesson будут заполнены далее, по ходу работы
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 41);
    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj);
        if (obj == null)
            return false;

        if (!(obj instanceof Lesson))
            return false;

        if (this.id != ((Lesson) obj).getId()) {
            // проверка идентичности Lesson по полю id
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        SimpleDateFormat format1 = new SimpleDateFormat("dd MM yyyy");
        return name;
        //        return "id = " + id +
        //                "; SerialVersionUUID = " + SerialVersionUUID +
        //                "; name = " + name +
        //                "; startTime = " + format1.format(startTime.getTime()) +
        //                "; duration = " + duration +
        //                "; room = " + room +
        //                "; description = " + description +
        //                "; subject = " + subject +
        //                "; teacher = " + teacher;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getStartTime() {
        return startTime;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public UUID getSerialVersionUUID() {
        return SerialVersionUUID;
    }

    public HashMap<Long, Group> getGroups() {
        return groups;
    }
}
