package com.std.students_andr.students_andr.models.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBStudent {
    private static final String DB_NAME = "studentdb";
    private static final int DB_VERSION = 1;
    private static final String DB_TABLE = "students";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FIRST_NAME = "firstname";
    public static final String COLUMN_SURNAME = "surname";
    public static final String COLUMN_SECOND_NAME = "secondname";
    //firstname TEXT, surname TEXT, secondname

//    public static final String COLUMN_IMG = "img";
//    public static final String COLUMN_TXT = "txt";

    public static final String DBStudent_CREATE = "create table " + DB_TABLE + "(" +
            COLUMN_ID + " long primary key autoincrement, " +
            //COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_FIRST_NAME + " firstname" +
            COLUMN_SURNAME + " surname" +
            COLUMN_SECOND_NAME + " secondname" +
            //COLUMN_IMG + " integer, " +
            //COLUMN_TXT + " text" +
            ");";
    //db.execSQL("CREATE TABLE students(id LONG PRIMARY KEY AUTOINCREMENT, firstname TEXT, surname TEXT, secondname TEXT);");

    private final Context mCtx;
    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    public DBStudent(Context context) {
        mCtx = context;
    }

    // открыть подключение
    public void open() {
        mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    // закрыть подключение
    public void close() {
        if (mDBHelper != null) mDBHelper.close();
    }

    // получить все данные из таблицы DB_TABLE
    public Cursor getAllData() {
        return mDB.query(DB_TABLE, null, null, null, null, null, null);
    }

    // добавить запись в DB_TABLE
    public void addRec(String firstname, String surname, String secondname) {
        //public void addRec(String txt, int img) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_FIRST_NAME, firstname);
        cv.put(COLUMN_SURNAME, surname);
        cv.put(COLUMN_SECOND_NAME, secondname);
        mDB.insert(DB_TABLE, null, cv);
        //COLUMN_IMG + " integer, " +
        //COLUMN_TXT + " text" +
    }

    // удалить запись из DB_TABLE
    public void delRec(long id) {
        mDB.delete(DB_TABLE, COLUMN_ID + " = " + id, null);
    }

}
