package com.std.students_andr.students_andr.adapters;

import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.models.Lesson;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RVAdapterJournal extends RecyclerView.Adapter<RVAdapterJournal.JournalViewHolder> {
    //private List<Pair<Lesson, Boolean>> lessonsPresents = new ArrayList<>();
    private List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents = new ArrayList<>();

    public RVAdapterJournal(List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents) {
        //public RVAdapterJournal(HashMap<Lesson, Boolean> presentStudents) {
        //        for (HashMap.Entry<Lesson, Boolean> entrySt : presentStudent.entrySet()) {
        //            Pair<Lesson, Boolean> pair = new Pair<>(entrySt.getKey(), entrySt.getValue());
        //            lessonsPresents.add(pair);
        //        }

        //        List<Pair<Pair<Student, Lesson>, Boolean>> lessonsPresents = new ArrayList<>();
        //        for (HashMap.Entry<Student, HashMap<Lesson, Boolean>> entry : presentHashAll.entrySet())
        //            for (HashMap.Entry<Lesson, Boolean> entryLes : entry.getValue().entrySet()) {
        //                Pair<Student, Lesson> pairStLes = new Pair<>(entry.getKey(), entryLes.getKey());
        //                Pair<Pair<Student, Lesson>, Boolean> pairPres = new Pair<>(pairStLes, entryLes.getValue());
        //                lessonsPresents.add(pairPres);
        //            }

        this.lessonsPresents = lessonsPresents;
    }

    public class JournalViewHolder extends RecyclerView.ViewHolder {
        TextView student;
        TextView lesson;
        TextView present;

        public JournalViewHolder(final View itemView) {
            super(itemView);
            student = (TextView) itemView.findViewById(R.id.tvJournalItemStudent);
            lesson = (TextView) itemView.findViewById(R.id.tvJournalItemLesson);
            present = (TextView) itemView.findViewById(R.id.tvJournalItemPresent);
        }
    }

    @Override
    public JournalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.journal_list_item, parent, false);
        return new JournalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(JournalViewHolder holder, int position) {
        //holder.bind(mGroupList.get(position));
        Pair<Student, Lesson> pairStLes = lessonsPresents.get(position).first;
        String str_present = (lessonsPresents.get(position).second) ? "присутств." : "отсутств.";
        holder.student.setText(pairStLes.first.getSurname().toString());
        holder.lesson.setText(pairStLes.second.toString());
        holder.present.setText(str_present);

        //        String str_present = (lessonsPresents.get(position).second) ? "присутствовал" : "отсутствовал";
        //        holder.lesson.setText(lessonsPresents.get(position).first.toString());
        //        holder.present.setText(str_present);

        // привязка экземпляра объекта студента к строке списка
        //holder.bind(students.get(position));
    }

    //    @Override
//    public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_item, parent, false);
//        return new StudentViewHolder(v);
//    }
//    @Override
//    public long getItemId(int position) {
//        return students.get(position).getId();
//    }
//    @Override
//    public void onBindViewHolder(StudentViewHolder holder, int position) {
//        //holder.bind(mGroupList.get(position));
//        holder.studentFirstName.setText(students.get(position).getFirstName());
//        holder.studentSurname.setText(students.get(position).getSurname());
//        holder.studentSecondName.setText(students.get(position).getSecondName());
//        // привязка экземпляра объекта студента к строке списка
//        //holder.bind(students.get(position));
//    }
    @Override
    public int getItemCount() {
        return lessonsPresents.size();
    }
}
