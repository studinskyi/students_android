package com.std.students_andr.students_andr.managers;

import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Lesson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ManagerLessons {
    private Manager manager;
    private HashMap<Long, Lesson> lessons;

    public ManagerLessons(Manager manager) {
        this.manager = manager;
        this.lessons = new HashMap<>();
    }

    // реализация синглтона менеджера через класс-холдер
    public ManagerLessons() {
        this.lessons = new HashMap<>();
    }

    private static class ManagerLessonsHolder {
        private final static ManagerLessons instance = new ManagerLessons();
    }

    public static ManagerLessons getInstance() {
        return ManagerLessonsHolder.instance;
    }

    public Lesson addLesson(String name, Calendar startTime, Integer duration,
                            String room, String description, String subject, String lector, Group group) {
        Lesson lesson = new Lesson(name, startTime, duration, room, description, subject, lector, group);
        //        // добавление лессона, если он ещё не зафиксирован
        //if (lesson != null && lessons.get(lesson.getId()) == null)
        this.lessons.put(lesson.getId(), lesson);
        return lesson;
    }

    public HashMap<Long, Lesson> addLesson(Lesson lesson) {
        //if (lesson != null && lessons.get(lesson.getId()) == null)
        this.lessons.put(lesson.getId(), lesson);
        return lessons;
    }

    public List<Lesson> getLessonsListAll() {
        //        List<Lesson> listLes = new ArrayList<>();
        //        for (HashMap.Entry<Long, Lesson> entry : lessons.entrySet())
        //                listLes.add(entry.getValue());
        List<Lesson> listLes = new ArrayList<Lesson>(lessons.values());
        return listLes;
    }

    public List<Lesson> getLessonListForGroup(Group group) {
        List<Lesson> listLes = new ArrayList<>();
        for (HashMap.Entry<Long, Lesson> entryLes : lessons.entrySet())
            for (HashMap.Entry<Long, Group> entryGr : entryLes.getValue().getGroups().entrySet())
                if (entryGr.getValue().equals(group)) { // если в списку групп занятия присутствует данная группа
                    listLes.add(entryLes.getValue());
                    break; // сброс прохода для анализа групп, привязанных к следующему занятию
                }
        return listLes;
    }

    public List<Lesson> getLessonListFilteredByName(String nameLes) {
        List<Lesson> listLes = new ArrayList<>();
        for (HashMap.Entry<Long, Lesson> entry : lessons.entrySet())
            if (entry.getValue().getName().contains(nameLes))
                listLes.add(entry.getValue());
        return listLes;
    }


    public Manager getManager() {
        return manager;
    }

    public HashMap<Long, Lesson> getLessons() {
        return lessons;
    }

    public Lesson getLessonById(Long lessonId) {
        return lessons.get(lessonId);
    }
}
