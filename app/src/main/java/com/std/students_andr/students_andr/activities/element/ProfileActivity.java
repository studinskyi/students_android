package com.std.students_andr.students_andr.activities.element;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.activities.list.JournalListActivity;
import com.std.students_andr.students_andr.activities.list.StudentsActivity;
import com.std.students_andr.students_andr.models.Contact;
import com.std.students_andr.students_andr.models.ContactType;
import com.std.students_andr.students_andr.models.Student;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvFirstName;
    private TextView tvSurname;
    private TextView tvSecondName;
    private TextView tvPhone;
    private TextView tvTelegram;
    private TextView tvSkype;
    private Button btnGroupProfileStudent;
    private Button btnEditProfileStudent;
    private Button btnDeleteProfileStudent;

    private Student choiceStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //choiceStudent = RVAdapterStudent.getChoiceStudent();
        // получение параметра studentId из родительского активити
        Intent intent = getIntent();
        choiceStudent = MainActivity.manager.getStudentById(intent.getLongExtra("studentId", 0));

        tvFirstName = (TextView) findViewById(R.id.tvFirstName);
        tvSurname = (TextView) findViewById(R.id.tvSurname);
        tvSecondName = (TextView) findViewById(R.id.tvSecondName);
        tvPhone = (TextView) findViewById(R.id.tvPhone);
        tvTelegram = (TextView) findViewById(R.id.tvTelegram);
        tvSkype = (TextView) findViewById(R.id.tvSkype);
        btnGroupProfileStudent = (Button) findViewById(R.id.btnGroupProfileStudent);
        btnEditProfileStudent = (Button) findViewById(R.id.btnEditProfileStudent);
        btnDeleteProfileStudent = (Button) findViewById(R.id.btnDeleteProfileStudent);

        btnGroupProfileStudent.setOnClickListener(this);
        btnEditProfileStudent.setOnClickListener(this);
        btnDeleteProfileStudent.setOnClickListener(this);

        tvFirstName.setText(choiceStudent.getFirstName());
        tvSurname.setText(choiceStudent.getSurname());
        tvSecondName.setText(choiceStudent.getSecondName());

        MainActivity.manager.get_mStudents().addContactStudent(ContactType.PHONE, "+7(888)1234567", choiceStudent);
        MainActivity.manager.get_mStudents().addContactStudent(ContactType.TELEGRAM, "user_telegram", choiceStudent);
        MainActivity.manager.get_mStudents().addContactStudent(ContactType.SKYPE, "user_skype", choiceStudent);
        MainActivity.manager.get_mStudents().addContactStudent(ContactType.EMAIL, "user@gmail.com", choiceStudent);
        MainActivity.manager.get_mStudents().addContactStudent(ContactType.VK, "vk_user", choiceStudent);

        for (Contact contact : choiceStudent.getContacts()) {
            switch (contact.getType()) {
                case PHONE:
                    tvPhone.setText(contact.getValue());
                    break;
                case TELEGRAM:
                    tvTelegram.setText(contact.getValue());
                    break;
                case SKYPE:
                    tvSkype.setText(contact.getValue());
                    break;
            }
        }
    }

    public void clickOpenJournal(View view) {
        JournalListActivity.choiceStudent = choiceStudent;
        startActivity(new Intent(getBaseContext(), JournalListActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGroupProfileStudent:
                //StudentsActivity.setChoiceGroup(MainActivity.manager.getGroupByStudent());
                Intent intentStudentList = new Intent(getBaseContext(), StudentsActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                //Intent intentStudentList = new Intent(getBaseContext(), StudentsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //Intent intentStudentList = new Intent(getBaseContext(), StudentsActivity.class);
                intentStudentList.putExtra("substrSurnameStudent", ""); // подстрока фильтра студентов
                intentStudentList.putExtra("nameChoiseGroup", MainActivity.manager.getGroupByStudent(choiceStudent).getName()); // передача имени группы для отбора студентов
                startActivity(intentStudentList);
                //moveTaskToBack()
                break;
            case R.id.btnEditProfileStudent:
                // edit profile student
                break;
            case R.id.btnDeleteProfileStudent:
                // delete profile student
                break;
        }
    }
}
