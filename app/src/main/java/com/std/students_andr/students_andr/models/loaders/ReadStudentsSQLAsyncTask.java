package com.std.students_andr.students_andr.models.loaders;

import android.content.Context;
import android.os.AsyncTask;

import com.std.students_andr.students_andr.activities.list.StudentListActivity;
import com.std.students_andr.students_andr.managers.Manager;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.List;

public class ReadStudentsSQLAsyncTask extends AsyncTask<String, Integer, List<Student>> {
    // где переопределяется абстрактный класс
    // android.os.AsyncTask<Params,Progress,Result>
    // String - соответствует блоку входны параметров задачи Params и передаётся в метод doInBackground(String... strings)
    // Integer - соответствует блоку Progress и передаётся в метод onProgressUpdate(Integer... values)
    // List<Student> - соответствует блоку Result и передаётся в метод
    Context context;

    public ReadStudentsSQLAsyncTask(Context context) {
        this.context = context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (context != null) {
            //Toast.makeText(context, "onPreExecute", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPostExecute(List<Student> listStudents) {
        // Double - соответствует блоку Result и передаётся в метод
        super.onPostExecute(listStudents);
        //Toast.makeText(context, "Result count students is: " + listStudents.size(), Toast.LENGTH_SHORT).show();
        StudentListActivity.setListStudents(listStudents); // обновляем студентов в активити списка студентов
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // Integer - соответствует блоку Progress
        super.onProgressUpdate(values);
        if (context != null) {
            //Toast.makeText(context, "onProgressUpdate: " + values[0], Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected List<Student> doInBackground(String... params) {
        String nameGroup = params[0];
        String substrFirstNameStudent = params[1];
        List<Student> listStudents = Manager.getInstance().get_mStudents().getStudentsListAllFromDB();

        // фильтрация списка студентов по подстроке
        if (!substrFirstNameStudent.equals("") && substrFirstNameStudent != null) {
            List<Student> listFiltr = new ArrayList<>();
            for (Student student : listStudents)
                if (student.getFirstName().contains(substrFirstNameStudent))
                    listFiltr.add(student);

            listStudents = listFiltr;
            //            listStudents.clear();
            //            for (Student student : listFiltr)
            //                listStudents.add(student);
        }

        //        List<Student> listStudents = new ArrayList<>();
        //        DBHelper helper = new DBHelper(context, "studentdb", null, 1);
        //        SQLiteDatabase database = helper.getWritableDatabase();
        //        Cursor cursor = database.query("students", null, null, null, null, null, null);
        //        Student student = null;
        //        while (cursor.moveToNext()) {
        //            student = Manager.getInstance().get_mStudents().addStudent2(cursor.getString(cursor.getColumnIndex("firstname"))
        //                    , cursor.getString(cursor.getColumnIndex("surname"))
        //                    , cursor.getString(cursor.getColumnIndex("secondname")));
        //            listStudents.add(student);
        //        }
        //        helper.close();
        return listStudents;
    }
}
