package com.std.students_andr.students_andr.managers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.std.students_andr.students_andr.models.Contact;
import com.std.students_andr.students_andr.models.ContactType;
import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Student;
import com.std.students_andr.students_andr.models.sqlite.DBHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ManagerStudents {
    private Manager manager;
    Context context;

    public ManagerStudents(Manager manager, Context context) {
        this.manager = manager;
        this.context = context;
    }

    // реализация синглтона менеджера через класс-холдер
    public ManagerStudents() {
    }

    private static class ManagerStudentsHolder {
        private final static ManagerStudents instance = new ManagerStudents();
    }

    public static ManagerStudents getInstance() {
        return ManagerStudentsHolder.instance;
    }


    public Student addStudent(Calendar dateOfBirth, Long groupID, String firstName,
                              String surname, String secondName) {
        // если студент с этим ФИО не найден то можно добавить его
        Student studentNew = getStudent(firstName, surname, secondName);
        if (studentNew == null)
            studentNew = new Student(dateOfBirth, groupID, firstName, surname, secondName);

        return studentNew; // если студент уже имелся в базе, то возвращается ссылка на этот объект
    }

    public Student addStudent2(String firstName,
                               String surname, String secondName) {

        return new Student(firstName, surname, secondName); // если студент уже имелся в базе, то возвращается ссылка на этот объект
    }


    public List<Contact> addContactStudent(ContactType contactType, String contactValue, Student student) {
        // добавление контактов студента
        List<Contact> contacts = student.getContacts();
        Contact contactNew = new Contact(contactType, contactValue);
        contacts.add(contactNew);
        return contacts;
    }

    public List<Contact> getContacts(Student student) {
        return student.getContacts();
    }

    public int getAmountOfStudnts() {
        int counStudents = 0;
        HashMap<Long, Group> groups = manager.get_mGroups().getGroups();
        // проход по всем группам и накопление их студентов
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            counStudents = counStudents + entry.getValue().getStudents().size();

        return counStudents;
    }

    //    public boolean addStudent(Student student, int groupNum) {
    //        Long groupId = getGroupId(groupNum);
    //        if (groupId == -1) return false;
    //        for (Group group : manager.get_mGroups().getGroups()) {
    //            if (group.getId().equals(groupId)) {
    //                addStudent(student, group);
    //                return true;
    //            }
    //        }
    //        return false;
    //    }

    public Student getStudent(String firstName, String surname, String secondName) {
        Student student;
        HashMap<Long, Group> groups = manager.get_mGroups().getGroups();
        // проход по всем группам и сверка ФИО студента
        for (HashMap.Entry<Long, Group> entryGr : groups.entrySet())
            for (HashMap.Entry<Long, Student> entrySt : entryGr.getValue().getStudents().entrySet()) {
                student = entrySt.getValue();
                if (student.getFirstName().equals(firstName) &&
                        student.getSecondName().equals(secondName) &&
                        student.getSurname().equals(surname))
                    return student;
            }

        return null;
    }

    public List<Student> getStudentsListAll() {
        //        List<Student> listSt = new ArrayList<>();
        //        DBHelper helper = new DBHelper(context, "studentdb", null, 1);
        //        SQLiteDatabase database = helper.getWritableDatabase();
        //        Cursor cursor = database.query("students", null, null, null, null, null, null);
        //        Student student = null;
        //        while (cursor.moveToNext()) {
        //            student = addStudent2(cursor.getString(cursor.getColumnIndex("firstname"))
        //                    , cursor.getString(cursor.getColumnIndex("surname"))
        //                    , cursor.getString(cursor.getColumnIndex("secondname")));
        //            listSt.add(student);
        //        }
        List<Student> listSt = new ArrayList<>();
        for (HashMap.Entry<Long, Group> entry : manager.get_mGroups().getGroups().entrySet())
            for (HashMap.Entry<Long, Student> entrySt : entry.getValue().getStudents().entrySet())
                listSt.add(entrySt.getValue());
        //helper.close();
        return listSt;
    }

    public List<Student> getStudentsListAllFromDB() {
        List<Student> listSt = new ArrayList<>();
        DBHelper helper = new DBHelper(context, "studentdb", null, 1);
        SQLiteDatabase database = helper.getWritableDatabase();
        Cursor cursor = database.query("students", null, null, null, null, null, null);
        Student student = null;
        while (cursor.moveToNext()) {
            student = addStudent2(cursor.getString(cursor.getColumnIndex("firstname"))
                    , cursor.getString(cursor.getColumnIndex("surname"))
                    , cursor.getString(cursor.getColumnIndex("secondname")));
            listSt.add(student);
        }
        helper.close();
        return listSt;
    }

    public List<Student> getStudentListFilteredByFirstName(String firstName) {
        List<Student> listSt = new ArrayList<>();
        for (Student student : getStudentsListAll())
            if (student.getFirstName().contains(firstName))
                listSt.add(student);
        return listSt;
    }

    public List<Student> getStudentListFilteredBySurname(String surname) {
        List<Student> listSt = new ArrayList<>();
        for (Student student : getStudentsListAll())
            if (student.getSurname().contains(surname))
                listSt.add(student);
        return listSt;
    }

    // добавление списка студентов в гуппу
    public HashMap<Long, Student> addStudentsToGroup(Group group, List<Student> listStudents) {
        HashMap<Long, Student> studentsInGroup = group.getStudents();
        if (listStudents.size() > 0) {
            for (Student student : listStudents)
                manager.addStudentToGroup(student, group);
        }
        return group.getStudents(); // возврат результирующего HashMap студентов группы
    }


    // функция проверки наличия студента в группе
    public boolean isStudentInGroup(Group group, Student student) {
        HashMap<Long, Student> students = group.getStudents();
        if (students.get(student.getId()) != null)
            return true; // студент присутствует в списке группы
        return false; // студент отсутствует в списке группы
    }


    public void removeStudent(Student student) {
        //for (Group group : manager.get_mGroups().getGroups()) {
        Group group = manager.get_mGroups().getGroups().get(student.getGroupID());
        group.getStudents().remove(student.getId());
        //        HashMap<Long, Student> students = manager.get_mGroups().getGroups().get(student.getGroupID()).getStudents();
        //        for (HashMap.Entry<Long, Student> entrySt : students.entrySet()) {
        //            if (student.equals(entrySt.getValue())) { // объект студента найден в списке группы
        //                students.remove(student.getId());
        //                return true; // истина в случае удаления студента из списка
        //            }
        //        }
        //        return false;
    }

    public Student getStudentById(Long studentId) {
        for (HashMap.Entry<Long, Group> entry : manager.get_mGroups().getGroups().entrySet())
            for (HashMap.Entry<Long, Student> entrySt : entry.getValue().getStudents().entrySet())
                if (entrySt.getValue().getId().equals(studentId))
                    return entrySt.getValue();

        return null;
    }

    @Override
    public String toString() {
        return manager.get_mGroups().getGroups().toString();
    }
}
