package com.std.students_andr.students_andr.activities.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.activities.MainActivity;
import com.std.students_andr.students_andr.activities.add.AddStudentActivity;
import com.std.students_andr.students_andr.fragments.FragmentStudentList;
import com.std.students_andr.students_andr.models.Group;
import com.std.students_andr.students_andr.models.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentsActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    FragmentStudentList fragmentStudentList;
    private SearchView svSurnameFilter;
    private TextView tvCaptionStudents;
    public static Group choiceGroup;
    Menu menu_student_list;
    private List<Student> listStudents = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);
        Log.d(MainActivity.getLogTAG(), "onCreate - activity_students");

        fragmentStudentList = (FragmentStudentList) getFragmentManager().findFragmentById(R.id.fragmentStudentList);
        svSurnameFilter = (SearchView) findViewById(R.id.svSurnameFilter);
        svSurnameFilter.setOnQueryTextListener(this);
        // получение параметра подстроки фильтрации из родительского активити
        Intent intent = getIntent();
        svSurnameFilter.setQuery(intent.getStringExtra("substrSurnameStudent"), false);
        //svSurnameFilter.clearFocus(); // убрать фокус с поля поиска
        choiceGroup = MainActivity.manager.getGroupByNameGroup(intent.getStringExtra("nameChoiseGroup"));

        tvCaptionStudents = (TextView) findViewById(R.id.tvCaptionStudents);
        if (MainActivity.isUserAdministrator())
            tvCaptionStudents.setText("Список студентов из групп");
        else
            tvCaptionStudents.setText("студенты группы " + choiceGroup.getName());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(MainActivity.getLogTAG(), "onNewIntent - activity_students");
        // получение параметра подстроки фильтрации из родительского активити
        svSurnameFilter.setQuery(intent.getStringExtra("substrSurnameStudent"), false);
        //svSurnameFilter.clearFocus(); // убрать фокус с поля поиска
        choiceGroup = MainActivity.manager.getGroupByNameGroup(intent.getStringExtra("nameChoiseGroup"));
    }

    private void filterListStudents(String surnameSubstr) {
        listStudents.clear();
        if (MainActivity.isUserAdministrator() && choiceGroup == null) {
            if (surnameSubstr.equals("") || surnameSubstr == null)
                listStudents.addAll(MainActivity.manager.getStudentsListAll());
            else
                listStudents.addAll(MainActivity.manager.getStudentListFilteredBySurname(surnameSubstr));
        } else {
            // если всё же определена группа, то выборку студентов нужно делать именно по ней
            for (Student student : MainActivity.manager.getStudentsListInGroup(choiceGroup)) {
                if (!surnameSubstr.equals("") && !(surnameSubstr == null)) // фильтрация по подстроке surnameSubstr
                    if (!student.getSurname().contains(surnameSubstr)) // строка фильтра surnameSubstr задана, но не найдена в элементе
                        continue;

                listStudents.add(student);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(MainActivity.getLogTAG(), "onResume - activity_students");
        refreshListInFragment();
    }

    public void refreshListInFragment() {
        Log.d(MainActivity.getLogTAG(), "refreshListInFragment - activity_students");
        // учет строки фильтра при выводе списка групп
        filterListStudents(svSurnameFilter.getQuery().toString()); // определение общей выборки студентов и фильтрация
        fragmentStudentList.setListStudents(listStudents);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(MainActivity.getLogTAG(), "onCreateOptionsMenu - activity_students");
        this.menu_student_list = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_students_list, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //Log.d(MainActivity.getLogTAG(), "onPrepareOptionsMenu - activity_students");
        //this.menu_student_list.setGroupVisible(,);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAddStudent:
                Log.d(MainActivity.getLogTAG(), "Нажата кнопка AddStudent в activity_students");
                startActivity(new Intent(getBaseContext(), AddStudentActivity.class));
                return true;
            case R.id.miShowAllStudents:
                Log.d(MainActivity.getLogTAG(), "Нажата кнопка ShowAllStudents в activity_students");
                if (MainActivity.isUserAdministrator()) {
                    choiceGroup = null;
                    svSurnameFilter.setQuery("", false);
                    refreshListInFragment();
                } else {
                    Toast.makeText(this, "Список всех пользователей доступен только администраторам!", Toast.LENGTH_SHORT).show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static Group getChoiceGroup() {
        return choiceGroup;
    }

    public static void setChoiceGroup(Group choiceGroup) {
        StudentsActivity.choiceGroup = choiceGroup;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // User pressed the search button
        Log.d(MainActivity.getLogTAG(), "onQueryTextSubmit - activity_students");
        refreshListInFragment();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // User changed the text
        Log.d(MainActivity.getLogTAG(), "onQueryTextChange - activity_students");
        refreshListInFragment();
        return false;
    }
}
