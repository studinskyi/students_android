package com.std.students_andr.students_andr.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.std.students_andr.students_andr.activities.element.ProfileActivity;
import com.std.students_andr.students_andr.R;
import com.std.students_andr.students_andr.models.Student;

import java.util.List;

public class RVAdapterStudent extends RecyclerView.Adapter<RVAdapterStudent.StudentViewHolder> {
    private List<Student> listStudents;
    private static Student choiceStudent;

    public RVAdapterStudent(List<Student> listStudents) {
        this.listStudents = listStudents;
    }

    public class StudentViewHolder extends RecyclerView.ViewHolder {
        TextView studentFirstName;
        TextView studentSurname;
        TextView studentSecondName;
        private ActionMode mActionMode;
        //Student student;

        public StudentViewHolder(final View itemView) {
            super(itemView);
            studentFirstName = (TextView) itemView.findViewById(R.id.tvStudentItemFirstName);
            studentSurname = (TextView) itemView.findViewById(R.id.tvStudentItemSurname);
            studentSecondName = (TextView) itemView.findViewById(R.id.tvStudentItemSecondName);

            // 1-й вариант вызова контекстного меню через "Использование режима контекстных действий"
            final ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.menu_sms_call, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                // Called when the user selects a contextual menu item
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    Toast.makeText(itemView.getContext(), "выбран пункт меню " + listStudents.get(getAdapterPosition()).getSurname(), Toast.LENGTH_SHORT).show();
                    switch (item.getItemId()) {
                        case R.id.miSendSMS:
                            Intent intentSendSMS = new Intent(Intent.ACTION_SENDTO);
                            intentSendSMS.setData(Uri.parse("smsto:+79124277403"));
                            intentSendSMS.putExtra("sms_body", "text sms");
                            intentSendSMS.resolveActivity(itemView.getContext().getPackageManager());
                            itemView.getContext().startActivity(intentSendSMS);
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        case R.id.miCall:
                            Intent intentCall = new Intent(Intent.ACTION_DIAL);
                            intentCall.setData(Uri.parse("tel:+79102366669"));
                            intentCall.resolveActivity(itemView.getContext().getPackageManager());
                            itemView.getContext().startActivity(intentCall);
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    mActionMode = null;
                }
            };

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                // Called when the user long-clicks on someView
                public boolean onLongClick(View view) {
                    if (mActionMode != null) {
                        return false;
                    }

                    // Start the CAB using the ActionMode.Callback defined above
                    mActionMode = ((Activity) itemView.getContext()).startActionMode(mActionModeCallback);
                    view.setSelected(true);
                    return true;
                }
            });

            // 2-й вариант вызова контекстного меню через переопределение  itemView.setOnCreateContextMenuListener
            //            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            //                @Override
            //                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            //                    MenuItem itemCall = menu.add("call " + listGroups.get(getAdapterPosition()).getName());
            //                    itemCall.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            //                        @Override
            //                        public boolean onMenuItemClick(MenuItem item) {
            //                            Intent myIntent = new Intent(Intent.ACTION_DIAL);
            //                            myIntent.setData(Uri.parse("tel:+79102366669"));
            //                            myIntent.resolveActivity(itemView.getContext().getPackageManager());
            //                            itemView.getContext().startActivity(myIntent);
            //                            return true;
            //                        }
            //                    });
            //                    //Toast.makeText(itemView.getContext(), "выбран пункт меню " + listGroups.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
            //                }
            //            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    choiceStudent = listStudents.get(pos);
                    Intent intentProfileStudent = new Intent(itemView.getContext(), ProfileActivity.class);
                    //Intent intentProfileStudent = new Intent(itemView.getContext(), ProfileActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentProfileStudent.putExtra("studentId", choiceStudent.getId()); // передача id студента в активити профиля студента
                    itemView.getContext().startActivity(intentProfileStudent);
                    //itemView.getContext().startActivity(new Intent(itemView.getContext(), ProfileActivity.class));
                }
            });

        }

        //        public void bind(Student student) {
        //            this.student = student;
        //            //choiceStudent = student;
        //            studentFirstName.setText(student.getFirstName());
        //            studentSurname.setText(student.getSurname());
        //            studentSecondName.setText(student.getSecondName());
        //
        //        }
    }

    //    @Override
    //    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    //        super.onAttachedToRecyclerView(recyclerView);
    //    }

    @Override
    public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_item, parent, false);
        return new StudentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(StudentViewHolder holder, int position) {
        //holder.bind(mGroupList.get(position));
        holder.studentFirstName.setText(listStudents.get(position).getFirstName());
        holder.studentSurname.setText(listStudents.get(position).getSurname());
        holder.studentSecondName.setText(listStudents.get(position).getSecondName());
        // привязка экземпляра объекта студента к строке списка
        //holder.bind(listStudents.get(position));
    }

    @Override
    public long getItemId(int position) {
        return listStudents.get(position).getId();
        //return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return listStudents.size();
    }

    public static Student getChoiceStudent() {
        return choiceStudent;
    }
}
