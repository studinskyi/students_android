package com.std.students_andr.students_andr.models.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {


    public DBHelper(Context context, String name,
                    SQLiteDatabase.CursorFactory factory,
                    int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(DBStudent.DBStudent_CREATE);
        db.execSQL("CREATE TABLE students(id LONG PRIMARY KEY AUTOINCREMENT, firstname TEXT, surname TEXT, secondname TEXT);");
        //        ContentValues cv = new ContentValues();
        //        for (int i = 1; i < 5; i++) {
        //            cv.put(COLUMN_TXT, "sometext " + i);
        //            cv.put(COLUMN_IMG, R.drawable.ic_launcher);
        //            db.insert(DB_TABLE, null, cv);
        //        }



        //db.execSQL("DROP TABLE IF EXISTS  students;"); // удаление базы если она уже существует на момент использования DBHelper
        //db.execSQL("DROP TABLE students;"); // удаление базы если она уже существует на момент использования DBHelper
        //        db.execSQL("INSERT INTO students (firstname,surname,secondname) VALUES('Michail','Michailov','Michailovich');");
        //        db.execSQL("INSERT INTO students (firstname,surname,secondname) VALUES('M11','ich11','lo11');");
        //        db.execSQL("INSERT INTO students (firstname,surname,secondname) VALUES('M22','ich22','lo22');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}
